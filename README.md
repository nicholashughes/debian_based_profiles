# Debian-based Profile Setup
Configures a Debian-based OS using a profile. <br>
Requires python >= 3.6.

<a name="table-of-contents" id="table-of-contents"></a>
## Table of Contents
- [Quickstart](#quickstart)
- [Overview](#overview)
  - [Install](#install)
  - [Uninstall](#uninstall)
  - [Dryrun](#dryrun)
- [Profiles](#profiles)
  - [Profile Fields](#profile-fields)
- [Development Notes](#development-notes)

<a name="quickstart" id="quickstart"></a>
## Quickstart
To install the profile, follow these instructions and then run the script.
```
cd ~
mkdir -p os_setup
cd os_setup
sudo apt install git
git clone https://bitbucket.org/nicholashughes/debian_based_profiles
cd debian_based_profiles

./install_profile.sh
# Or provide a profile from the 'profiles' dir
./install_profile -p <profile>
./install_profile --profile <profile>
```

#### Debian quickstart notes
 Debian server does not come with `sudo` installed, though the desktop version does. Debian users also
 do not start with `sudo` permissions. For the quickstart to work, the following instructions must be
 run first:

 ```
 # Become root
 su -
 # Install sudo and git
 apt-get install sudo git -y
 # Add user to 'sudo' group
 usermod -aG sudo <username>
 # Logout/login in headless or reboot to be added to group
 # Perform the quickstart instructions
```


<a name="overview" id="overview"></a>
## Overview
This project configures a Debian-based OS through settings outlined in a profile JSON file.
They can be installed/removed via their respective scripts
(see the [Quickstart](#quickstart)) section.

The following can be configured:

  - Install base profiles, recursively
  - Update and upgrade the system, optionally perform a `dist-upgrade`
  - Reset the `dconf` settings
  - Install Apt software packages
  - Install Snap software packages
  - Install a dotfiles repo, as long as it contains an `.install.sh`
  - Install Docker
  - Install Direnv
  - Install Pyenv
  - Install Google Chrome
  - Configure Solarized settings
  - Set Launcher shortcuts
  - Set Launcher favorites
  - Set gsettings
  - Enable/Disable apport
  - Enable numlock at login
  - Set IPv4 or IPv6 preference
  - Perform any other commands at the end
  - Cleanup the system
  - Show any reminders to perform after the install is complete

**Notes**

  - A backup of the current `dconf` settings is always made and saved to `~/.dconf_backups/`.
  - Installing dotfiles takes a GitHub URL as a value, and will be cloned or pulled. The
    project must have an `install.sh` script for it to be installed, or it will fail.
  - The `launcher_shortcuts` section installs shortcuts from the list included in `data/shortcuts` and
    provides shortcuts to Google Drive, Google Maps, Youtube, etc.
  - The `launcher_favorites` comes after installing software, so it is possible to
    include icons for software that has yet to be installed.
  - The `gsettings` is handled near the end, so any specified gsettings here will overwrite any other
    gsettings. This means it can overwrite Gedit gsettings as well as the Unity Launcher favorites.

#### Install
```
./install_profile.sh
```
Arguments:

  - *-h* or *--help*: The help command to show how to run the install.
  - *-p* or *--profile*: The profile to automatically install (no prompt).
  - *--use*: The parts of the profile to install, if only certain parts are desired.
  - *--skip*: The parts of the profile to skip, if only certain parts are desired.

Installing the profiles with the `./install_profile.sh` script will
parse the profile JSON file and configure the OS based on the options
outlined therein.

A log of the install process is written to file `install_profile_<timestamp>.log`.

A backup of the current `dconf` settings is made and written to `~/.dconf_backups/dconf_settings_<timestamp>.dconf`.

**Note:** It is not necessary to create the `os_setup` directory, but it is helpful to have a parent directory
for the other directories that will be created.
```
cd ~
sudo apt install git
mkdir -p os_setup
cd os_setup
git clone https://bitbucket.org/nicholashughes/debian_based_profiles
cd debian_based_profiles
```

#### Uninstall
**NOTE: The uninstallation of a profile cannot reset to the state before a profile was installed.**
```
./remove_profile.sh
```

Arguments:

  - *-h* or *--help*: The help command to show how to run the uninstaller.
  - *-p* or *--profile*: The profile to remove (no prompt).
  - *--use*: The parts of the profile to remove, if only certain parts are desired.
  - *--skip*: The parts of the profile to skip, if only certain parts are desired.

Removing the profile with the `remove_profile.sh` script will
reset or uninstall any settings outlined in the profile. This
is not idempotent, so it will not reset any settings to what
they were, but rather to their OS defaults. This is also
true for software that was installed, but also included in the
profile. It will be uninstalled, even if it had been installed
previous to installing the profile, meaning you may have to
reinstall it.

A log of the uninstall process is written to file `remove_profile_<timestamp>.log`.

#### Dryrun
**NOTE: There is no dryrun Bash script shortcut, instead call the script directly.**
```
cd utils
python3 setup_profile.py dryrun
```

Arguments:

  - *-h* or *--help*: The help command to show how to run the dryrun.
  - *-p* or *--profile*: The profile to dryrun.

Running the dryrun command will essentially build out the final profile that will
be installed. As a profile recursively builds from base profiles, it may not be
clear what the final profile may look like. This dryrun command build out that
final profile and then prints it to the screen.

<a name="profiles" id="profiles"></a>
## Profiles
A description of the profiles that are included within this project.
These profiles are found in the `profiles` directory.

| Profiles                      | Description                      |
| ----------------------------- | -------------------------------- |
| debian-headless               | Debian headless |
| debian-headless-developer     | Debian headless with developer software |
| debian-headless-laptop        | Debian headless for a laptop |
| debian-headless-media-editing | Debian headless with media editing software |
| debian-gui                    | Debian GUI |
| debian-gui-developer          | Debian GUI with developer software |
| debian-gui-laptop             | Debian GUI for a laptop |
| debian-gui-media-editing      | Debian GUI with media editing software |
| debian-gui-vm                 | Debian GUI for a virtual machine |
| ubuntu-headless               | Ubuntu headless |
| ubuntu-headless-developer     | Ubuntu headless with developer software |
| ubuntu-headless-laptop        | Ubuntu headless for a laptop |
| ubuntu-headless-media-editing | Ubuntu headless with media editing software|
| ubuntu-headless-ppa           | Ubuntu headless with certain PPAs|
| ubuntu-gui                    | Ubuntu GUI |
| ubuntu-gui-developer          | Ubuntu GUI with developer software |
| ubuntu-gui-laptop             | Ubuntu GUI for a laptop  |
| ubuntu-gui-media-editing      | Ubuntu GUI with media editing software |
| ubuntu-gui-ppa                | Ubuntu GUI with certain PPAs |
| personal-angelika-ubuntu-gui  | Designed for Angelika |
| personal-federica-ubuntu-gui  | Designed for Federica |
| personal-franco-ubuntu-gui    | Designed for Franco   |
| personal-nicholas-ubuntu-gui  | Designed for Nicholas |

<a name="profile-fields" id="profile-fields"></a>
#### Profile fields
The valid profile fields can be found in `utils/validate_profile.py`. if more fields are added to a profile,
and the fields are not updated here, the profile will fail validation. *Note, this only validates the top-level
fields. Fields that contain a dict will not have the individual fields within the dict validated.*

**Fields:**

  - **apt_software** [list of dicts or list of strings] - The APT packages to install. This can contain string or dict entries, and can be mixed and matched.
    If it is a string entry, it must be the name of the package.
    If it is a dict, this is the valid format:
      - **name** (**required**) - Otherwise it will not be installed.
      - **description** (optional) - Usually the dpkg comment for the package, but can be anything.
      - **repo** (optional) - If present the PPA will default to being installed.
      - **use_repo** (optional) - If set to 'false', the PPA repo will not be installed, even if the 'repo' key is
        present. This is for situations where the repo does not yet have a package entry for the specified version of
        Ubuntu. Rather than forcing a user to delete the repo key, this can be used instead.
      - **pre** (optional) - Any commands to be issued BEFORE the package is installed.
      - **post** (optional) - Any commands to be issued AFTER the package is installed.
      - **requires_manual_input** (optional) - Notifies that this requires manual input and is queued to be installed
        first. This way the rest of the install profile process can be left to run alone.
      - **comment** (optional) - Any comments. As a JSON file, this is pretty much the only way to have comments.
  - **base_profiles** [list of strings] - The base profiles to inherit from.
  - **cleanup_system** [bool] - Will 'cleanup' the system by installing any missing packages and removing any unused packages.
  - **configure_solarized** [bool] - This will configure Ethan Schooner's Solarized color settings for Gedit, a Terminal profiles,
    and general shell output colors.
  - **direnv** [dict] - Whether to run the `setup_direnv.py` script. This will install `direnv` APT package.
  - **docker** [dict] - Whether to run the `setup_docker.py` script. This will install `docker`.
  - **dotfiles** [string] - The value here must be a GitHub repo with an `install.sh`. Examples include mine or say one of the
    multiple GitHub dotfiles repos that use an `install.sh`.
  - **enable_apport** [bool] - Whether or not to enable apport. Ubuntu defaults this to enabled, but most people like it disabled,
    hence the 'false' value in most profiles.
  - **enable_numlock** [bool] - Whether or not enable numlock upon login. Ubuntu defaults this to enabled, but other OSes do not.
  - **gsettings** [list of lists] - A list of gsettings values to configure.
  - **google_chrome** [bool] - Whether to run the `setup_google_chrome.py` script. This will install the Google Chrome browser.
  - **ip_preference** [string] - If set to 'IPv4' (or 'ipv4'), will change the `/etc/gai.conf` to prefer IPv4. By default Ubuntu
    sets this to prefer IPv6. using IPv6 used to be a problem with updating packages, but now seems to be resolved.
  - **launcher_favorites** [list of strings] - A list of shortcuts to appear on the launcher. These must be their gsetting values.
  - **launcher_shortcuts** [list of strings] - Any desired shortcuts, such as Google Maps or YouTube to install. Valid options here are
    the shortcuts found in `data/shortcuts`. A `desktop` entry must exist for this to install. A 'png' icon must
    exists as well. Note that this will not automatically install create a launcher favorite, that must be done in
    `launcher_favorites` or manually.
  - **manual_reminders** [list of strings] - Any reminders to be performed manually after the configuration has completed.
  - **other_commands** [list of strings] - Any other commands to run at the end.
  - **pyenv** [dict] - Whether to run the `setup_pyenv.py` script. This will install `pyenv`.
  - **reset_dconf** [bool] - Whether to reset the dconf settings back to their OS defaults. Leave this key out or set this
    value to 'false' to skip resetting the dconf settings. Note, a backup is made of the current dconf settings
    regardless of this setting and stored to `~/.dconf_backups/dconf_settings_<timestamp>.dconf`
  - **snap_software** [list of strings] - The Snap packages to install. They must be listed individually to support the option to install
    a 'classic' Snap package.
  - **update_system** [string] - Whether to run `sudo apt-get update && sudo apt-get upgrade -y`. If the value is set to 'true',
    it will run `sudo apt-get update && sudo apt-get dist-upgrade -y`. To skip updating, leave this key out.

<a name="development notes" id="development-notes"></a>
## Development Notes
If any other third party installers are created, similar to the Docker, Pyenv, and Google Chrome
installers, be sure to add in the same area of the `install_os_profile` function in `setup_profile.py`.
This ensures that the settings are run after any software is installed, and specifically, allows
for launcher icons to be included after the software is installed. Otherwise the software might not
be found and may not have the icon.

The `configure_solarized` function sets the `Terminal` and `Gedit` to use the Solarized Dark
color scheme. Thus in the `gsettings` section, the schema must be overridden if that is
not the desired theme. This is why `gsettings` are set after `configure_solarized`.

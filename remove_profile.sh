#!/usr/bin/env bash

# Reset OS using the profile
time=$(date +%s)
python3 utils/setup_profile.py remove "$@" &> >(tee remove_profile_"$time".log)

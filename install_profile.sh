#!/usr/bin/env bash

# Configure OS using the profile
time=$(date +%s)
python3 utils/setup_profile.py install "$@" &> >(tee install_profile_"$time".log)

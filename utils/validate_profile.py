import argparse
import json
import os

VALID_PROFILE_DATA = {
    'apt_software': list,
    'base_profiles': list,
    'cleanup_system': bool,
    'configure_solarized': bool,
    'direnv': dict,
    'docker': dict,
    'dotfiles': str,
    'enable_apport': bool,
    'enable_numlock': bool,
    'gsettings': list,
    'google_chrome': bool,
    'ip_preference': str,
    'launcher_favorites': list,
    'launcher_shortcuts': list,
    'manual_reminders': list,
    'other_commands': list,
    'pyenv': dict,
    'reset_dconf': bool,
    'snap_software': list,
    'update_system': str,
}
VALID_PROFILE_KEYS = {entry for entry in VALID_PROFILE_DATA.keys()}


def _get_project_path():
    """Get the path to the project directory."""
    file_path = os.path.realpath(__file__)
    utils_path = os.path.dirname(file_path)
    project_path = os.path.dirname(utils_path)

    return project_path


def _get_profiles_path():
    """Get the path to the profiles directory."""
    project_path = _get_project_path()
    profiles_path = os.path.join(project_path, 'profiles')

    return profiles_path


VALID_PROFILES = sorted([os.path.splitext(name)[0]
                         for name in os.listdir(_get_profiles_path())
                         if name.endswith('.json')])


class StoreSelectedProfilesAction(argparse.Action):
    """An argparse Action class for storing the selected Profiles.

    If a selected profile name is not valid, it will raise an error.
    A valid entry can include the .json extension or not.

    By default, all profiles are selected for validation.

    """
    def __init__(self, option_strings, dest, **kwargs):
        kwargs['default'] = VALID_PROFILES
        kwargs['choices'] = VALID_PROFILES
        kwargs['nargs'] = '+'
        super().__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, set(values))


def parse_arguments():
    """Parse any arguments given for the script."""

    description = "Validate a setup profile."
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-p', '--profiles', metavar=('', ', '.join(VALID_PROFILES)),
                        action=StoreSelectedProfilesAction, help="Select the profile(s) to validate.")
    param_args = parser.parse_args()

    return param_args


def validate_profile(profile_name):
    """Validate a profile.

    Args:
        profile_name (str): The name of the profile to validate.

    Returns:
        bool: Whether the profile was valid or not.

    """
    # Check if profile exists - exit if not
    if not profile_name.endswith('.json'):
        profile_name += '.json'
    profile_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'profiles/', profile_name)
    if not os.path.isfile(profile_path):
        raise ValueError("Profile '{profile_name}' not found at path: '{profile_path}'")

    # Open the profile and begin to validate it
    print(f"-------Validating profile '{profile_name}'-------")
    with open(profile_path) as profile:
        profile_json = json.load(profile)
        # Validate the profile keys
        return validate_profile_data(profile_json)


def validate_profile_data(profile_data):
    """Validate profile keys.

    Args:
        profile_data (dict): The profile data to validate.

    Returns:
        bool: Whether the profile data was valid or not.

    """
    # Open the profile and begin to validate it

    # Validate the profile data type
    invalid_profile_keys = set()
    invalid_data_types = list()
    for k, v in profile_data.items():
        if k not in VALID_PROFILE_KEYS:
            invalid_profile_keys.add(k)
        elif type(v) != VALID_PROFILE_DATA[k]:
            invalid_data_types.append(f"Profile entry '{k}' is type '{type(v)}' but should be type '{VALID_PROFILE_DATA[k]}'")

    # Inform user of invalid keys and values
    if invalid_profile_keys or invalid_data_types:
        is_valid = False
        for invalid_profile_key in invalid_profile_keys:
            print(f"Invalid Profile Key: {invalid_profile_key}")
        for invalid_data_type in invalid_data_types:
            print(invalid_data_type)
    else:
        is_valid = True
        print(f"Profile data is valid")

    return is_valid


def get_all_json_profiles():
    """Gets all the JSON profiles.

    Returns:
        list: A list containing the all profile names in the 'profiles' directory.

    """
    profile_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'profiles/')
    profiles = [f for f in os.listdir(profile_path)
                if os.path.isfile(os.path.join(profile_path, f)) and f.endswith('.json')]

    return profiles


if __name__ == '__main__':
    """The 'main' program."""
    args = parse_arguments()

    for p in args.profiles:
        validate_profile(p)
        print()

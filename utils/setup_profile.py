import argparse
import datetime
import json
import os
import sys
import subprocess
from urllib.parse import urlsplit

import os_utils
from git_utils import git_clone_or_pull_repo
from setup_direnv import install_direnv, uninstall_direnv
from setup_docker import install_docker, uninstall_docker
from setup_numlock_on_login import enable_numlock_on_login, remove_numlock_on_login
from setup_google_chrome import install_google_chrome, uninstall_google_chrome
from setup_pyenv import install_pyenv, uninstall_pyenv
from validate_profile import VALID_PROFILE_KEYS, validate_profile, validate_profile_data, _get_project_path, _get_profiles_path


class StoreSelectedProfileKeysAction(argparse.Action):
    """An argparse Action class for storing the selected Profile Keys.

    This will loop over all possible profile keys and either select or skip
    this part of the profile, depending on if the argument was '--use' or '--skip'.

    By default, the values are set to True, meaning that if this argument
    is not specified, then all profile parts will be selected.

    """
    DEFAULT_VALUES = {k: True for k in VALID_PROFILE_KEYS}

    def __init__(self, option_strings, dest, **kwargs):
        if dest != 'use':
            raise ValueError("The 'dest' value must be 'use'")
        kwargs['default'] = self.DEFAULT_VALUES
        super().__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        values_set = set(values)
        if option_string == '--use':
            use = {k: True if k in values_set else False for k in VALID_PROFILE_KEYS}
        elif option_string == '--skip':
            use = {k: False if k in values_set else True for k in VALID_PROFILE_KEYS}
        else:
            raise ValueError("Option string can only be '--use' or '--skip'")
        setattr(namespace, self.dest, use)


def parse_arguments():
    """Parse any arguments given for the script."""

    description = "Configure system via a profile."
    parser = argparse.ArgumentParser(description=description, add_help=False)
    parser.add_argument('-h', '--help', action='help', help="Show this help message and exit.")
    subparsers = parser.add_subparsers(help='Help with commands.', dest='subparser_name')

    # Install parser
    install_parser = subparsers.add_parser('install', help="Install a profile.", add_help=False)
    install_parser.add_argument('-h', '--help', action='help', help="Show this help message and exit.")
    install_parser.add_argument('-p', '--profile', dest='profile_name', help="The profile to install.")
    install_group = install_parser.add_mutually_exclusive_group()
    install_group.add_argument('--use', metavar=('CHOICES', ', '.join(sorted(VALID_PROFILE_KEYS))), choices=VALID_PROFILE_KEYS,
                               nargs='+', action=StoreSelectedProfileKeysAction,
                               help="The parts of the profile to install.")
    install_group.add_argument('--skip', metavar=('CHOICES', ', '.join(sorted(VALID_PROFILE_KEYS))), choices=VALID_PROFILE_KEYS,
                               nargs='+', action=StoreSelectedProfileKeysAction, dest='use',
                               help="The parts of the profile to skip installing.")

    # Remove parser
    remove_parser = subparsers.add_parser('remove', help="Remove a profile.", add_help=False)
    remove_parser.add_argument('-h', '--help', action='help', help="Show this help message and exit.")
    remove_parser.add_argument('-p', '--profile', dest='profile_name', help="The profile to remove.")
    remove_group = remove_parser.add_mutually_exclusive_group()
    remove_group.add_argument('--use', metavar=('CHOICES', ', '.join(VALID_PROFILE_KEYS)), choices=VALID_PROFILE_KEYS,
                              nargs='+', action=StoreSelectedProfileKeysAction,
                              help="The parts of the profile to remove.")
    remove_group.add_argument('--skip', metavar=('CHOICES', ', '.join(VALID_PROFILE_KEYS)), choices=VALID_PROFILE_KEYS,
                              nargs='+', action=StoreSelectedProfileKeysAction, dest='use',
                              help="The parts of the profile to skip removing.")

    # Generate full profile parser
    dryrun_parser = subparsers.add_parser('dryrun', help="Generate a dryrun profile.", add_help=False)
    dryrun_parser.add_argument('-h', '--help', action='help', help="Show this help message and exit.")
    dryrun_parser.add_argument('-p', '--profile', dest='profile_name', help="The profile to dryrun.")

    param_args = parser.parse_args()
    # If no subcommand was issued, print the help message
    if param_args.subparser_name is None:
        parser.print_help()
        sys.exit()

    return param_args


def get_valid_profile_path(profile_name):
    """Get the profile that will be installed.

    Validate that the profile exists in the list of profiles.
    If the profile was invalid or blank, it prompts the user for a
    valid profile until one is entered.

    Args:
        profile_name (str): The name of the profile from the profiles directory to install. The '.json'
            extension can be included or excluded.

    """
    # Get existing profiles
    profiles_dir = _get_profiles_path()
    existing_profiles = [os.path.splitext(entry)[0]
                         for entry in os.listdir(profiles_dir)
                         if os.path.splitext(entry)[1] == '.json']
    existing_profiles.sort()

    # If the 'profile_name' has a '.json' extension, drop the extension
    if profile_name:
        split_profile = os.path.splitext(profile_name)
        if split_profile[1] == '.json':
            profile_name = split_profile[0]

    if profile_name not in existing_profiles:
        print("\n---- Select Profile ----", flush=True)
        while profile_name not in existing_profiles:
            print("Valid profiles are: \n - {0}".format('\n - '.join(existing_profiles)), flush=True)
            profile_name = input("Please enter a valid profile: ")
            split_profile = os.path.splitext(profile_name)
            if split_profile[1] == '.json':
                profile_name = split_profile[0]
        print("Chosen profile: {0}".format(profile_name), flush=True)

    profile_path = os.path.join(profiles_dir, profile_name + '.json')

    return profile_path


def get_profile_path(profile_name):
    """The name of a profile.

    Args:
        profile_name (str): The name of an existing profile.

    Returns:
        str: The full path to the profile.

    """
    # If the 'profile_name' does not have a '.json' extension, add it
    if not profile_name.endswith('.json'):
        profile_name += '.json'
    # Get the profiles directory and append the profile name
    profile_path = os.path.join(_get_profiles_path(), profile_name)
    return profile_path


def _build_base_profiles(profile_name, base_profiles):
    """Build the list of base profiles.

    This will recursively go through and get all the base profiles
    of each profile. For example, ubuntu-gui.json specifies
    debian-gui.json, which in turn specifies debian-headless.json
    as base profiles. This will generate that list even meaning
    that a higher level profile need not specify each base profile.

    Args:
        profile_name (str): The name of the profile.
        base_profiles (list): The list of profiles. This is modified
            as this function is called.

    """
    # Turn the profile name into a path
    profile_path = get_profile_path(profile_name=profile_name)
    with open(profile_path, 'r') as profile_file:
        profile_json = json.load(profile_file)

    # Add the base profiles found into the list if they are not present
    for base_profile in profile_json.get('base_profiles', []):
        # A list is used instead of a set to preserver order
        if base_profile not in base_profiles:
            if not base_profile.endswith('.json'):
                base_profile += '.json'
            base_profiles.append(base_profile)

    # Now recurse and add lower level base profiles
    for base_profile in profile_json.get('base_profiles', []):
        _build_base_profiles(base_profile, base_profiles)


def _sort_os_profiles(os_name, os_profiles):
    """Sort OS profiles with custom logic.

    This sorts the profiles with the following logic:
    - The 'personal' profiles come first, though there should not be more
      than one personal profile.
    - The 'ubuntu' profiles come next, selecting the PPA profiles, if present.
      This is to be sure that any packages specified here take precedence over
      any specified in any other profile. After this the 'gui' profiles take
      precendence over the 'headless' profiles.
    - The 'debian' profiles come next. As above, the 'gui' profiles come before
      the 'headless' profiles.

    Args:
        os_name (str): The name of the OS, such as 'Ubuntu' or 'Debian'.
        os_profiles (list): The profiles to be sorted.

    Returns:
        list: The sorted 'os_profiles'.

    """
    os_name = os_name.lower()
    sorted_os_profiles = list()
    try:
        profile_index = os_profiles.index(f'{os_name}-gui-ppa.json')
    except ValueError:
        pass  # This is ok, it means it wasn't found
    else:
        sorted_os_profiles.append(os_profiles.pop(profile_index))
    try:
        profile_index = os_profiles.index(f'{os_name}-headless-ppa.json')
    except ValueError:
        pass  # This is ok, it means it wasn't found
    else:
        sorted_os_profiles.append(os_profiles.pop(profile_index))
    os_gui_profiles = list()
    os_headless_profiles = list()
    for entry in os_profiles:
        if entry.startswith(f'{os_name}-gui'):
            os_gui_profiles.append(entry)
        elif entry.startswith(f'{os_name}-headless'):
            os_headless_profiles.append(entry)
        else:
            raise ValueError(f"Cannot sort unknown profile type: {entry}")
    sorted_os_profiles.extend(os_gui_profiles)
    sorted_os_profiles.extend(os_headless_profiles)

    return sorted_os_profiles


def _sort_base_profiles(base_profiles):
    """Sort the list of base profiles via hierarchy.

    Different profiles contain different entries and because the
    list is built recursively, it is important to ensure that the
    final profile is built correctly. This will go through and sort
    the profiles, ensuring that base profiles are added in correct
    order.

    Specifically, the profiles should be organized in this way:
    - The personal profile
    - The 'ubuntu' profiles, as Ubuntu is built on Debian
    - The 'debian' profiles, as this is the base layer

    Args:
        base_profiles (list): The list of profiles. This is modified
            as this function is called.

    """
    ordered_base_profiles = list()
    personal_profiles = []
    ubuntu_profiles = []
    debian_profiles = []
    for entry in base_profiles:
        if entry.startswith('personal-'):
            personal_profiles.append(entry)
        elif entry.startswith('ubuntu-'):
            ubuntu_profiles.append(entry)
        elif entry.startswith('debian-'):
            debian_profiles.append(entry)
        else:
            raise ValueError(f'Unknown profile type: {entry}')
    ordered_base_profiles.extend(personal_profiles)

    # Sort the Ubuntu profiles
    if ubuntu_profiles:
        ordered_base_profiles.extend(_sort_os_profiles('ubuntu', ubuntu_profiles))
    if debian_profiles:
        ordered_base_profiles.extend(_sort_os_profiles('debian', debian_profiles))

    # Double check to be sure all values in base_profiles are in ordered_base_profiles
    set_ordered_base_profiles = set(ordered_base_profiles)
    set_base_profiles = set(base_profiles)
    missing_profiles = set_base_profiles.difference(set_ordered_base_profiles)
    if missing_profiles:
        raise ValueError(f"Ordered base profiles does not match original base_profiles: {missing_profiles}")
    return ordered_base_profiles


def _merge_base_profiles(profile_json):
    """Merge the base profiles into the profile data.

    This generates the JSON that will be installed. It loops over each
    base profile and adds features not found in previous profiles,
    allowing for profiles to be modular.

    Note:
        It is possible that a boolean value may collide with another. As
        it stands, a boolean value will only be added if it is not present.
        To avoid the scenario where two profiles perhaps have a conflicting
        value, set it in a personal profile. This way, the value is already
        set at the highest level profile.

    Args:
         profile_json (dict): The JSON dict to start with. This will be modified
             by this function.

    """
    # Build base profiles into one profile
    for base_profile in profile_json.get('base_profiles'):
        base_profile_path = get_profile_path(profile_name=base_profile)
        with open(base_profile_path, 'r') as base_profile_file:
            base_profile_json = json.load(base_profile_file)
            # APT software
            if 'apt_software' in base_profile_json:
                # Create an entry for 'apt_software' if it does not exist
                if 'apt_software' not in profile_json:
                    profile_json['apt_software'] = list()
                # Add entries and ensure no duplicates get in
                profile_apt_keys = set([entry['name'] for entry in profile_json['apt_software']])
                for entry in base_profile_json['apt_software']:
                    if entry['name'] not in profile_apt_keys:
                        profile_json['apt_software'].append(entry)
                        profile_apt_keys.add(entry['name'])
                    else:
                        print(f"Skipping apt_software {entry['name']} from {base_profile_file.name}")
            # Clean up system
            if 'cleanup_system' in base_profile_json and 'cleanup_system' not in profile_json:
                profile_json['cleanup_system'] = base_profile_json['cleanup_system']
            # Configure solarized
            if 'configure_solarized' in base_profile_json and 'configure_solarized' not in profile_json:
                profile_json['configure_solarized'] = base_profile_json['configure_solarized']
            # Direnv
            if 'direnv' in base_profile_json and 'direnv' not in profile_json:
                profile_json['direnv'] = base_profile_json['direnv']
            # Docker
            if 'docker' in base_profile_json and 'docker' not in profile_json:
                profile_json['docker'] = base_profile_json['docker']
            # Dotfiles
            if 'dotfiles' in base_profile_json and 'dotfiles' not in profile_json:
                profile_json['dotfiles'] = base_profile_json['dotfiles']
            # Enable apport
            if 'enable_apport' in base_profile_json and 'enable_apport' not in profile_json:
                profile_json['enable_apport'] = base_profile_json['enable_apport']
            # Enable numlock
            if 'enable_numlock' in base_profile_json and 'enable_numlock' not in profile_json:
                profile_json['enable_numlock'] = base_profile_json['enable_numlock']
            # Gsettings
            if 'gsettings' in base_profile_json:
                # Create an entry for 'gsettings' if it does not exist
                if 'gsettings' not in profile_json:
                    profile_json['gsettings'] = list()
                # Add entries and ensure no duplicates get in
                gsettings_keys = set([entry[0]+' '+entry[1] for entry in profile_json['gsettings']])
                for entry in base_profile_json['gsettings']:
                    current_key = entry[0]+' '+entry[1]
                    if current_key not in gsettings_keys:
                        profile_json['gsettings'].append(entry)
                        gsettings_keys.add(current_key)
                    else:
                        print(f"Skipping gsettings {entry} from {base_profile_file.name}")
            # Google Chrome
            if 'google_chrome' in base_profile_json and 'google_chrome' not in profile_json:
                profile_json['google_chrome'] = base_profile_json['google_chrome']
            # IP preference
            if 'ip_preference' in base_profile_json and 'ip_preference' not in profile_json:
                profile_json['ip_preference'] = base_profile_json['ip_preference']
            # Launcher favorites
            if 'launcher_favorites' in base_profile_json and 'launcher_favorites' not in profile_json:
                profile_json['launcher_favorites'] = base_profile_json['launcher_favorites']
            # Launcher shortcuts
            if 'launcher_shortcuts' in base_profile_json and 'launcher_shortcuts' not in profile_json:
                profile_json['launcher_shortcuts'] = base_profile_json['launcher_shortcuts']
            # Manual reminders
            if 'manual_reminders' in base_profile_json:
                if 'manual_reminders' in profile_json:
                    for entry in base_profile_json['manual_reminders']:
                        if entry not in profile_json['manual_reminders']:
                            profile_json['manual_reminders'].append(entry)
                else:
                    profile_json['manual_reminders'] = base_profile_json['manual_reminders']
            # Other commands
            if 'other_commands' in base_profile_json:
                if 'other_commands' in profile_json:
                    for entry in base_profile_json['other_commands']:
                        if entry not in profile_json['other_commands']:
                            profile_json['other_commands'].append(entry)
                else:
                    profile_json['other_commands'] = base_profile_json['other_commands']
            # Pyenv
            if 'pyenv' in base_profile_json and 'pyenv' not in profile_json:
                profile_json['pyenv'] = base_profile_json['pyenv']
            # Snap software
            if 'snap_software' in base_profile_json:
                if 'snap_software' in profile_json:
                    for entry in base_profile_json['snap_software']:
                        if entry not in profile_json['snap_software']:
                            profile_json['snap_software'].append(entry)
                else:
                    profile_json['snap_software'] = base_profile_json['snap_software']
            # Reset dconf settings
            if 'reset_dconf' in base_profile_json and 'reset_dconf' not in profile_json:
                profile_json['reset_dconf'] = base_profile_json['reset_dconf']
            # Update system
            if 'update_system' in base_profile_json and 'update_system' not in profile_json:
                profile_json['update_system'] = base_profile_json['update_system']


def build_profile(profile_name, use=None):
    # Get the profile to use
    profile_path = get_valid_profile_path(profile_name=profile_name)
    print("\n---- Build profile {0} ----".format(profile_path), flush=True)
    # Reset profile name to the one from the profile_path
    profile_name = os.path.split(profile_path)[1]

    # Get the profile data
    with open(profile_path, 'r') as profile_file:
        profile_json = json.load(profile_file)
    # Generate all the base profiles
    base_profiles = list()
    _build_base_profiles(profile_name, base_profiles)
    # Order the base profiles by hierarchy
    base_profiles = _sort_base_profiles(base_profiles)
    # Update the 'base_profiles' of the profile
    profile_json['base_profiles'] = base_profiles
    # Build the profile by merging all the base profiles into the profile
    _merge_base_profiles(profile_json)
    # Sort the apt_software
    if profile_json.get('apt_software'):
        profile_json['apt_software'].sort(key=lambda i: i['name'])
    # Sort the gsettings
    if profile_json.get('gsettings'):
        profile_json['gsettings'].sort()
    # Sort the snap software
    if profile_json.get('snap_software'):
        profile_json['snap_software'].sort()
    # Filter the manual reminders
    if profile_json.get('manual_reminders'):
        # Remove OS specific reminders if OS doesn't match
        os_entries = {'debian': 'Debian:', 'ubuntu': 'Ubuntu:', 'pop_os': 'Pop OS:', 'raspberry_pi_os': 'Raspberry Pi:'}
        os_entries_to_remove = [v.lower()
                                for k, v in os_entries.items()
                                if k != os_utils.get_distribution_name().lower()]
        # Loop over the manual reminders and exclude OS specific reminders if they are not for the current OS
        temp_manual_reminders = [entry
                                 for entry in profile_json['manual_reminders']
                                 if not entry[:entry.find(' ')].lower() in os_entries_to_remove]
        # Alphabetize the manual reminders
        profile_json['manual_reminders'] = sorted(temp_manual_reminders)

    # Validate the 'use' profile keys
    if use:
        invalid_keys = set(use.keys()).difference(VALID_PROFILE_KEYS)
        if invalid_keys:
            raise ValueError("Invalid profile keys: {0}".format(', '.join(invalid_keys)))
    else:
        use = StoreSelectedProfileKeysAction.DEFAULT_VALUES
    # Remove the parts of the profile that will not be used
    for k in list(profile_json.keys()):
        if not use.get(k):
            del profile_json[k]

    # Validate the final profile data
    validate_profile_data(profile_json)

    return profile_json


def install_os_profile(profile_name, use=None):
    """Install a profile.

    Args:
        profile_name (str): The name of the profile from the profiles directory to install. The '.json'
            extension can be included or excluded.
        use (dict or None): A dict containing the selected parts of profile and whether or not to
            install them. Defaults to None, which will use the entire profile.

    """
    # Generate the profile to install
    profile_json = build_profile(profile_name, use)

    print("---- Setup System ----", flush=True)
    print("{0}".format(datetime.datetime.now().strftime('%b-%d-%Y %H:%M:%S')), flush=True)

    # Install profile settings
    print("\n---- Install profile {0} ----".format(profile_name), flush=True)
    print("Profile data\n{0}".format(json.dumps(profile_json, indent=4, sort_keys=True)), flush=True)

    # Update and upgrade the system
    if 'update_system' in profile_json:
        if profile_json['update_system'] == 'upgrade':
            os_utils.system_update(dist_upgrade=False)
        elif profile_json['update_system'] == 'dist-upgrade':
            os_utils.system_update(dist_upgrade=True)
        else:
            raise ValueError("The update_system value must be 'upgrade' or 'dist-upgrade'")

    # Backup dconf before making changes to it
    backup_dconf()
    # Reset dconf before making changes to it
    if profile_json.get('reset_dconf'):
        os_utils.DconfUtils.reset_dconf()

    # Install apt software
    if 'apt_software' in profile_json:
        install_apt_software(profile_json['apt_software'])
    # Install snap software
    if 'snap_software' in profile_json:
        install_snap_software(profile_json['snap_software'])
    # Install dotfiles into the directory where this project is located
    if 'dotfiles' in profile_json:
        install_dotfiles(profile_json['dotfiles'], os.path.dirname(_get_project_path()))
    # Install docker
    if 'docker' in profile_json:
        install_docker(**profile_json['docker'])
    # Install direnv
    if 'direnv' in profile_json:
        install_direnv(**profile_json['direnv'])
    # Install pyenv
    if 'pyenv' in profile_json:
        install_pyenv(**profile_json['pyenv'])
    # Install google chrome
    if 'google_chrome' in profile_json:
        install_google_chrome()
    # Install launcher shortcuts - this must come before 'launcher_favorites' as launcher favorites may include
    # these shortcuts
    if 'launcher_shortcuts' in profile_json:
        install_launcher_shortcuts(profile_json['launcher_shortcuts'])
    # Configure the solarized settings - do this before gsettings because if we specify a solarized background,
    # or something similar, it will fail
    if profile_json.get('configure_solarized'):
        configure_solarized()
    # Configure launcher icons
    if 'launcher_favorites' in profile_json:
        os_utils.GnomeLauncherFavoriteUtils.set_launcher_favorites(profile_json['launcher_favorites'])
    # Configure gsettings - do this near the end so that it's changes are not overridden
    if 'gsettings' in profile_json:
        print("\n---- Configure Gsettings ----", flush=True)
        for preference in profile_json['gsettings']:
            os_utils.GSettingsUtils.set_gsetting(preference[0], preference[1], preference[2])
    # Configure apport
    if 'enable_apport' in profile_json:
        os_utils.set_apport(profile_json['enable_apport'])
    # Enable numlock on login
    if 'enable_numlock' in profile_json:
        enable_numlock_on_login()
    # Configure IP preference, ie IPv6 (default) or IPv4
    if 'ip_preference' in profile_json:
        if profile_json['ip_preference'].lower() == 'ipv4':
            os_utils.enable_ipv4_preference()
        elif profile_json['ip_preference'].lower() == 'ipv6':
            os_utils.enable_ipv6_preference()
        else:
            raise ValueError(f"Invalid IP preference \'{profile_json['ip_preference']}\' - must be 'IPv4' or 'IPv6'")
    # Run any extra commands
    if 'other_commands' in profile_json:
        for command in profile_json['other_commands']:
            subprocess.run([command], shell=True, check=True)
    # Cleanup apt software
    if 'cleanup_system' in profile_json:
        os_utils.system_cleanup()
    # Get any reminders for manual operations
    if 'manual_reminders' in profile_json:
        print("\n---- Finally ----", flush=True)
        print("Do not forget to:\n{0}".format('\n'.join(profile_json.get('manual_reminders'))))

    print("\n---- Finished configuring system ----", flush=True)


def remove_os_profile(profile_name, use=None):
    """Remove a profile.

    Args:
        profile_name (str): The name of the profile from the profiles directory to remove. The '.json'
            extension can be included or excluded.
        use (dict or None): A dict containing the selected parts of profile and whether or not to
            remove them. Defaults to None, which will use the entire profile.

    """
    # Generate the profile to uninstall
    profile_json = build_profile(profile_name, use)

    print("---- Remove System Setup ----", flush=True)
    print("{0}".format(datetime.datetime.now().strftime('%b-%d-%Y %H:%M:%S')), flush=True)

    # Remove profile settings
    print("\n---- Remove profile {0} ----".format(profile_name), flush=True)
    print("Profile data\n{0}".format(json.dumps(profile_json, indent=4, sort_keys=True)), flush=True)

    # Backup dconf before making changes to it
    backup_dconf()
    # Reset dconf before making changes to it
    if profile_json.get('reset_dconf'):
        os_utils.DconfUtils.reset_dconf()
    # Remove apt software
    if 'apt_software' in profile_json:
        remove_apt_software(profile_json['apt_software'])
    # Remove snap software
    if 'snap_software' in profile_json:
        remove_snap_software(profile_json['snap_software'])
    # Remove dotfiles from the directory where this project is located
    if 'dotfiles' in profile_json:
        remove_dotfiles(os.path.dirname(_get_project_path()))
    # Uninstall docker
    if 'docker' in profile_json:
        uninstall_docker()
    # Uninstall direnv
    if 'direnv' in profile_json:
        uninstall_direnv()
    # Uninstall pyenv
    if 'pyenv' in profile_json:
        uninstall_pyenv()
    # Uninstall google chrome
    if 'google_chrome' in profile_json:
        uninstall_google_chrome()
    # Remove the numlock on login
    if 'enable_numlock' in profile_json:
        remove_numlock_on_login()
    # Configure apport back to the default (enabled)
    os_utils.set_apport(True)
    # Configure IP preference to the default (IPv6)
    os_utils.enable_ipv6_preference()
    # Cleanup apt software
    os_utils.system_cleanup()
    # Get any reminders for manual operations
    if 'manual_reminders' in profile_json:
        print("\n---- Finally ----", flush=True)
        print("Do not forget to undo:\n{0}".format('\n'.join(profile_json.get('manual_reminders'))))

    print("\n---- Finished removing profile ----", flush=True)


def install_apt_software(packages):
    """Installs software available through apt.

    Software that requires manual input is installed first. This is done so that
    the rest of the software can be installed unintended. No one wants to wait
    20 minutes for a user input screen to pop up; it would be better to get that
    over with at the beginning.

    Args:
        packages (list): A list of packages to install. Entries in the list
            can be a string or a dict. If they are a dict, they must contain
            the appropriate format.

    """
    print("\n---- Install Apt Software ----", flush=True)
    if not packages:
        print("No apt software selected to install")
        return

    pre_list_manual_input = list()
    repo_list_manual_input = list()
    install_list_manual_input = list()
    post_list_manual_input = list()
    pre_list = list()
    repo_list = list()
    install_list = list()
    post_list = list()

    if isinstance(packages, str):
        packages = [packages]

    for software in packages:
        if isinstance(software, dict):
            if software.get('requires_manual_input'):
                if 'pre' in software:
                    pre_list_manual_input.extend(software['pre'])
                # If there is a repo, use it unless it explicitly says 'use_repo': false
                if 'repo' in software and software.get('use_repo', True):
                    repo_list_manual_input.append(software['repo'])
                install_list_manual_input.append(software['name'])
                if 'post' in software:
                    post_list_manual_input.extend(software['post'])
            else:
                if 'pre' in software:
                    pre_list.extend(software['pre'])
                # If there is a repo, use it unless it explicitly says 'use_repo': false
                if 'repo' in software and software.get('use_repo', True):
                    repo_list.append(software['repo'])
                install_list.append(software['name'])
                if 'post' in software:
                    post_list.extend(software['post'])
        # The 'apt-software' key is a list, so it may contain a string entry
        if isinstance(software, str):
            install_list.append(software)

    # Remove duplicates
    pre_list_manual_input = list(set(pre_list_manual_input))
    repo_list_manual_input = sorted(list(set(repo_list_manual_input)))
    install_list_manual_input = sorted(list(set(install_list_manual_input)))
    post_list_manual_input = list(set(post_list_manual_input))
    pre_list = list(set(pre_list))
    repo_list = sorted(list(set(repo_list)))
    install_list = sorted(list(set(install_list)))
    post_list = list(set(post_list))

    manual_input_apt_group = dict(pre_list=pre_list_manual_input, repo_list=repo_list_manual_input,
                                  install_list=install_list_manual_input, post_list=post_list_manual_input)
    apt_group = dict(pre_list=pre_list, repo_list=repo_list, install_list=install_list, post_list=post_list)

    for install_group in (manual_input_apt_group, apt_group):
        repo_list = install_group.get('repo_list', [])
        pre_list = install_group.get('pre_list', [])
        install_list = install_group.get('install_list', [])
        post_list = install_group.get('post_list', [])

        print(f"repo list: {repo_list}")
        print(f"pre list: {pre_list}")
        print(f"install list: {install_list}")
        print(f"post list: {post_list}")

        # Add necessary repo PPAs and update
        if repo_list:
            print("\nInstall the following apt repositories: \n - {0}".format('\n - '.join(repo_list)), flush=True)
            # The add-apt-repository command can only take one repo at a time
            for repo in repo_list:
                subprocess.run(['sudo', 'add-apt-repository', '-y', '--no-update', repo], check=True)
            print("\nUpdating apt in quiet mode, please wait...", flush=True)
            subprocess.run(['sudo', 'apt-get', 'update', '-qq'], check=True)

        # Do any pre-install commands
        if pre_list:
            print("\nIssue these pre-install commands: \n - {0}".format('\n - '.join(pre_list)), flush=True)
            pre_commands = '(' + ';'.join(pre_list) + ')'
            subprocess.run([pre_commands], shell=True, check=True)

        # Install the apt software
        if install_list:
            print("\nInstall this software: \n - {0}".format('\n - '.join(install_list)), flush=True)
            subprocess.run(['sudo', 'apt-get', 'install', '-y', *install_list], check=True)

        # Do any post-install commands
        if post_list:
            print("\nIssue these post-install commands: \n - {0}".format('\n - '.join(post_list)), flush=True)
            for post_command in post_list:
                # The 'nautilus -q' command raises an error, so if it is the final command, an error will be raise.
                # Hence we turn off 'check' for this command, otherwise the install procedure will break here.
                if post_command.endswith('nautilus -q') or post_command.endswith('nautilus --quit'):
                    subprocess.run([post_command], shell=True, check=False)
                else:
                    subprocess.run([post_command], shell=True, check=True)


def remove_apt_software(packages):
    """Removes installed apt packages.

    Note:
        This will remove any related PPA repos. If that PPA is used for
        several packages, this will cause errors (minimally the software
        won't update anymore). This would only occur if calling this
        function manually, as generally this function is called when
        removing an entire profile.

    Args:
        packages (list): A list of packages to remove. Entries in the list
            can be a string or a dict. If they are a dict, they must contain
            the appropriate format.

    """
    print("\n---- Remove Apt Software ----", flush=True)
    if not packages:
        print("No apt software selected to remove")
        return

    repo_list = list()
    remove_list = list()

    if isinstance(packages, str):
        packages = [packages]

    for software in packages:
        if isinstance(software, dict):
            # If there is a repo, remove it unless it explicitly says 'use_repo': false
            if 'repo' in software and software.get('use_repo', True):
                repo_list.append(software['repo'])
            remove_list.append(software['name'])
        if isinstance(software, str):
            remove_list.append(software)

    # Remove duplicates
    repo_list = sorted(list(set(repo_list)))
    remove_list = sorted(list(set(remove_list)))

    print('repo list', repo_list)
    print('remove list', remove_list)

    # Remove the apt software
    if remove_list:
        print("\nRemove this software: \n - {0}".format('\n - '.join(remove_list)), flush=True)
        subprocess.run(['sudo', 'apt-get', 'remove', '-y', *remove_list], check=True)

    # Remove any installed repo PPAs and update
    if repo_list:
        print("\nRemove the following apt repositories: \n - {0}".format('\n - '.join(repo_list)), flush=True)
        for repo in repo_list:
            subprocess.run(['sudo', 'add-apt-repository', '-y', '--remove', repo], check=True)
        print("\nUpdating apt in quiet mode, please wait...", flush=True)
        subprocess.run(['sudo', 'apt-get', 'update', '-qq'], check=True)


def install_snap_software(software_list, apt_update=False):
    """Installs software available through snap.

    Args:
        software_list (list): A list of snap packages to install.
        apt_update (bool): Whether or not to run 'sudo apt-get update'.
            Defaults to False.

    """
    print("\n---- Install snap software ----", flush=True)
    # Install needed software
    if apt_update:
        subprocess.run(['sudo', 'apt-get', 'update'], check=True)
    subprocess.run(['sudo', 'apt-get', 'install', '-y', 'snapd'], check=True)
    for software in software_list:
        subprocess.run([f'sudo snap install {software}'], shell=True, check=True)


def remove_snap_software(software_list):
    """Removes installed snap software.

    Args:
        software_list (list): A list of snap packages to remove.

    """
    print("\n---- Remove snap software ----", flush=True)
    subprocess.run(['sudo', 'snap', 'remove', *software_list], check=True)


def install_dotfiles(dot_file_url, dot_file_path):
    """Installs a dotfile Git project.

    Note: This assumes that the git project has an 'install.sh' script for installing.

    Args:
        dot_file_url (str): The url of the git project enabling it to be cloned or pulled.
        dot_file_path (str): The path where the dot file project should go.

    """
    print("\n---- Install dotfiles  ----", flush=True)
    # Git clone or pull the project
    print("Get the dotfiles repo", flush=True)
    git_clone_or_pull_repo(dot_file_url, dot_file_path)

    # Run the installer - This assumes there is a file called 'install.sh'
    url_split = urlsplit(dot_file_url)
    url_project_path = os.path.split(url_split.path)[1]
    git_repo_name = os.path.splitext(url_project_path)[0]
    installer_path = os.path.join(dot_file_path, git_repo_name, 'install.sh')
    print("installer_path: {0}".format(installer_path), flush=True)
    if not os.path.isfile(installer_path):
        raise ValueError(f"File '{installer_path}' does not exist")
    subprocess.run([installer_path], check=True)


def remove_dotfiles(dot_file_path):
    """Removes a dotfile Git project.

    Note: This assumes that the git project has a 'remove.sh' script for uninstalling.

    Args:
        dot_file_path (str): The path where the dot file project is located.

    """
    print("\n---- Remove dotfiles  ----", flush=True)
    # Run the uninstaller - This assumes there is a file called 'remove.sh'
    uninstaller_path = os.path.join(dot_file_path, 'remove.sh')
    if not os.path.isfile(uninstaller_path):
        raise ValueError(f"File '{uninstaller_path}' does not exist")
    subprocess.run([uninstaller_path], check=True)


def install_launcher_shortcuts(launcher_shortcuts):
    """Installs shortcuts so they can be placed on the Unity Launcher bar.

    Args:
        launcher_shortcuts (list): List of shortcuts to install.

    """
    print("\n---- Install Unity Launcher Shortcuts ----", flush=True)
    for launcher in launcher_shortcuts:
        # Get the paths
        shortcuts_path = os.path.join(_get_project_path(), 'data', 'shortcuts')
        launcher_path = os.path.join(shortcuts_path, launcher+'.desktop')
        launcher_icon_path = os.path.join(shortcuts_path, launcher+'-icon.png')

        # Change the path to the icon file so that it does not have to be installed in
        # /usr/share/pixmaps
        subprocess.run(['sed', '-i', f's#^Icon\\=.*$#Icon\\={launcher_icon_path}#', f'{launcher_path}'],
                       check=True)

        # Validate and install the desktop file
        subprocess.run(['desktop-file-validate', launcher_path], check=True)
        subprocess.run(['desktop-file-install', f'--dir={os.path.expanduser("~")}/.local/share/applications/',
                        launcher_path], check=True)

        # Reset the changes that were made by sed
        subprocess.run(['git', 'checkout', os.path.join(shortcuts_path, '*')], check=True)


def remove_launcher_shortcuts(launcher_shortcuts):
    """Removes shortcuts from /usr/share/applications/.

    Args:
        launcher_shortcuts (list): List of launcher shortcuts to remove.

    """
    print("\n---- Remove Launcher Shortcuts ----", flush=True)
    for launcher in launcher_shortcuts:
        launcher_name = launcher+'.desktop'
        subprocess.run(['rm', '-f', '{0}/.local/share/applications/{1}'.format(os.path.expanduser('~'), launcher_name)],
                       check=True)
    os_utils.GnomeLauncherFavoriteUtils.remove_launcher_favorites([s + '.desktop' for s in launcher_shortcuts])


def backup_dconf(apt_update=False):
    """Backup the current dconf settings.

    Creates a timestamped backup of the current dconf settings
    and places it into ~/.dconf_backups. The backup file is
    dconf_settings_YYYY-MM-DD_HH-MM-SS.dconf.

    This will also install dconf, as it is required to backup
    the dconf settings.

    Args:
        apt_update (bool): Whether or not to run 'sudo apt-get update'.
            Defaults to False.

    """
    print("\n---- Backup dconf ----", flush=True)
    # Install needed software
    if apt_update:
        subprocess.run(['sudo', 'apt-get', 'update'], check=True)
    subprocess.run(['sudo', 'apt-get', 'install', '-y', 'dconf-cli', 'dconf-service', 'dconf-gsettings-backend'],
                   check=True)

    # Create backup directory, if necessary
    home_dir = os.path.expanduser('~')
    backup_dir = os.path.join(home_dir, '.dconf_backups')
    os.makedirs(backup_dir, exist_ok=True)

    # Generate backup filename with timestamp
    current_time = datetime.datetime.now()
    file_name = f'dconf_settings_{current_time.year}-{current_time.month}-{current_time.day}_{current_time.hour}-{current_time.minute}-{current_time.second}'

    # Dump dconf settings into backup directory
    backup_file = os.path.join(backup_dir, file_name)
    os_utils.DconfUtils.backup_dconf(backup_file)


def configure_solarized():
    """Configure solarized settings.

    This will set the colors displayed when using 'ls' commands.
    It will also configure Solarized settings for Terminal and Gedit.
    More information: http://ethanschoonover.com/solarized

    """
    project_parent_path = os.path.dirname(_get_project_path())

    print("\n---- Configure Solarized settings ----", flush=True)
    subprocess.run(['sudo', 'apt-get', 'install', '-y', 'git'], check=True)

    # Get solarized dircolors repository, which sets the colors displayed when doing
    # an 'ls' to use Solarized settings
    print("Get the dircolors-solarized repo", flush=True)
    git_clone_or_pull_repo('https://github.com/seebi/dircolors-solarized.git', project_parent_path)

    # Get solarized gnome terminal repository, which customizes the Terminal background
    # and colors to use Solarized settings
    print("Get the gnome-terminal-colors-solarized repo", flush=True)
    git_clone_or_pull_repo('https://github.com/Anthony25/gnome-terminal-colors-solarized.git', project_parent_path)

    # Get solarized gedit theme repository, which customizes the Gedit background
    # and colors to use Solarized settings
    print("Get the solarized-gedit theme repo", flush=True)
    git_clone_or_pull_repo('https://github.com/mattcan/solarized-gedit.git', project_parent_path)

    # Load these profiles, so that we have profiles that can be overridden with solarized profiles
    print("Load base terminal profiles", flush=True)
    default_terminal_path = os.path.join(_get_project_path(), 'data', 'default_terminal_profile.dconf')
    solarized_dark_terminal_path = os.path.join(_get_project_path(), 'data', 'solarized_dark_terminal_profile.dconf')
    solarized_dark2_terminal_path = os.path.join(_get_project_path(), 'data', 'solarized_dark2_terminal_profile.dconf')
    solarized_light_terminal_path = os.path.join(_get_project_path(), 'data', 'solarized_light_terminal_profile.dconf')
    wider_terminal_path = os.path.join(_get_project_path(), 'data', 'wider_terminal_profile.dconf')
    subprocess.run(['dconf load /org/gnome/terminal/legacy/profiles:/:b1dcc9dd-5262-4d8d-a863-c897e6d979b9/ < '
                    '{0}'.format(default_terminal_path)], shell=True, check=True)
    subprocess.run(['dconf load /org/gnome/terminal/legacy/profiles:/:b5550020-c71f-4b0b-acd5-2e24bf391659/ < '
                    '{0}'.format(solarized_dark_terminal_path)], shell=True, check=True)
    subprocess.run(['dconf load /org/gnome/terminal/legacy/profiles:/:cafdfba3-92e3-4b08-87fe-d391d479d583/ < '
                    '{0}'.format(solarized_dark2_terminal_path)], shell=True, check=True)
    subprocess.run(['dconf load /org/gnome/terminal/legacy/profiles:/:2ca8b1e0-4842-4cad-8e92-b1edbab3f359/ < '
                    '{0}'.format(solarized_light_terminal_path)], shell=True, check=True)
    subprocess.run(['dconf load /org/gnome/terminal/legacy/profiles:/:bf449cd5-ccde-4633-9032-307eff30cb20/ < '
                    '{0}'.format(wider_terminal_path)], shell=True, check=True)
    # Make all the profiles available in Terminal
    os_utils.GSettingsUtils.set_gsetting('org.gnome.Terminal.ProfilesList',
                                         'list',
                                         "['b1dcc9dd-5262-4d8d-a863-c897e6d979b9', "
                                         "'b5550020-c71f-4b0b-acd5-2e24bf391659', "
                                         "'cafdfba3-92e3-4b08-87fe-d391d479d583', "
                                         "'2ca8b1e0-4842-4cad-8e92-b1edbab3f359', "
                                         "'bf449cd5-ccde-4633-9032-307eff30cb20']")

    # Update to use the latest solarized schemes
    print("Load solarized terminal profiles", flush=True)
    gnome_terminal_colors_project = os.path.join(project_parent_path, 'gnome-terminal-colors-solarized')
    gnome_terminal_colors_installer = os.path.join(gnome_terminal_colors_project, 'install.sh')
    subprocess.run([gnome_terminal_colors_installer, '-s', 'dark', '-p', 'Solarized-Dark', '--skip-dircolors'],
                   check=True)
    subprocess.run([gnome_terminal_colors_installer, '-s', 'dark_alternative', '-p', 'Solarized-Dark2',
                    '--skip-dircolors'], check=True)
    subprocess.run([gnome_terminal_colors_installer, '-s', 'light', '-p', 'Solarized-Light', '--skip-dircolors'],
                   check=True)

    # Update the profiles in case the latest solarized schemes has changed them
    subprocess.run(['rm', '-f', '{0}'.format(solarized_dark_terminal_path),
                    '{0}'.format(solarized_dark2_terminal_path),
                    '{0}'.format(solarized_light_terminal_path)], check=True)
    subprocess.run(['dconf dump /org/gnome/terminal/legacy/profiles:/:b5550020-c71f-4b0b-acd5-2e24bf391659/ > '
                    '{0}'.format(solarized_dark_terminal_path)], shell=True, check=True)
    subprocess.run(['dconf dump /org/gnome/terminal/legacy/profiles:/:cafdfba3-92e3-4b08-87fe-d391d479d583/ > '
                    '{0}'.format(solarized_dark2_terminal_path)], shell=True, check=True)
    subprocess.run(['dconf dump /org/gnome/terminal/legacy/profiles:/:2ca8b1e0-4842-4cad-8e92-b1edbab3f359/ > '
                    '{0}'.format(solarized_light_terminal_path)], shell=True, check=True)

    # Set the default profile to Solarized-Dark2 for Debian
    if os_utils.get_distribution_name().lower() == 'debian':
        os_utils.GSettingsUtils.set_gsetting('org.gnome.Terminal.ProfilesList',
                                             'default',
                                             'cafdfba3-92e3-4b08-87fe-d391d479d583')
    # Set the default profile to Solarized-Dark for everything else
    else:
        os_utils.GSettingsUtils.set_gsetting('org.gnome.Terminal.ProfilesList',
                                             'default',
                                             'b5550020-c71f-4b0b-acd5-2e24bf391659')

    # Setup solarized gedit color schemes
    print("Setup solarized Gedit color schemes", flush=True)
    solarized_gedit_project = os.path.join(project_parent_path, 'solarized-gedit')
    solarized_gedit_styles = os.path.join(solarized_gedit_project, 'solarized-*')
    subprocess.run(['sudo', 'mkdir', '-p', '/usr/share/gtksourceview-3.0/styles'], check=True)
    subprocess.run(['sudo cp {0} /usr/share/gtksourceview-3.0/styles'.format(solarized_gedit_styles)], shell=True,
                   check=True)
    if os_utils.get_distribution_name().lower() == 'debian':
        os_utils.GSettingsUtils.set_gsetting('org.gnome.gedit.preferences.editor', 'scheme', 'solarizeddark')
    else:
        os_utils.GSettingsUtils.set_gsetting('org.gnome.gedit.preferences.editor', 'scheme', 'solarized-dark')


def main():
    args = parse_arguments()
    if args.subparser_name == 'install':
        install_os_profile(args.profile_name, args.use)
    elif args.subparser_name == 'remove':
        remove_os_profile(args.profile_name, args.use)
    elif args.subparser_name == 'dryrun':
        profile_json = build_profile(profile_name=args.profile_name)
        # Install profile settings
        print("\n---- Install profile {0} ----".format(args.profile_name), flush=True)
        print("Profile data\n{0}".format(json.dumps(profile_json, indent=4, sort_keys=True)), flush=True)


if __name__ == '__main__':
    """The 'main' program."""
    main()

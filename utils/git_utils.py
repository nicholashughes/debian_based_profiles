import os
import subprocess
from urllib.parse import urlsplit

from os_utils import is_package_installed


def get_latest_github_release_version(github_url=None, github_owner='', github_repo='', github_token=None):
    """Get the latest released tag name of a GitHub repository.

    Args:
        github_url (str): The url to the repository.
        github_owner (str): The owner of the repository.
        github_repo (str): The name of the repository.
        github_token (str): The token to use. This is generally for private repositories.

    Returns:
        str: The version name, ie 1.8.1.

    """
    if not is_package_installed('jq'):
        print("Installing 'jq' as this is required.", flush=True)
        subprocess.run(['sudo', 'apt-get', 'update'], check=True)
        subprocess.run(['sudo', 'apt-get', 'install', '-y', 'jq'], check=True)

    # Error check the parameters
    if github_url and (github_owner or github_repo):
        raise ValueError("The 'github_url' cannot be used along with the 'github_owner' and 'github_repo' parameters.")
    # Use the github_url to get the github_owner and github_repo
    if github_url:
        url_split = urlsplit(github_url)
        github_owner, github_repo = os.path.split(url_split.path)
        github_owner = github_owner.replace('/', '')
        # Drop the '.git', if we had one
        github_repo, git_project_ext = os.path.splitext(github_repo)
    # Else ensure we have a github_owner and github_repo
    elif not github_owner or not github_repo:
        raise ValueError("The 'github_url' cannot be used along with the 'github_owner' and 'github_repo' parameters.")

    # Build out the API url
    api_url = 'https://api.github.com/repos/{0}/{1}/releases/latest'.format(github_owner, github_repo)
    if github_token:
        command = 'curl -s -H "Authorization: token {0}" {1} | jq .tag_name'.format(github_token, api_url)
    else:
        command = 'curl -s {0} | jq .tag_name'.format(api_url)

    latest_release_data = subprocess.run([command], shell=True, check=True, stdout=subprocess.PIPE)
    version = latest_release_data.stdout.decode().strip().replace('"', '')
    return version


def git_clone_or_pull_repo(git_url, git_project_path='.', branch='master'):
    """Locally update a git project via git clone or git pull.

    Args:
        git_url (str): The git URL to either clone or pull.
        git_project_path (str): The path to either clone or pull to.
            Defaults to the current directory.
        branch (str): The branch to pull. Defaults to 'master'.

    """
    url_split = urlsplit(git_url)
    git_owner, git_project = os.path.split(url_split.path)
    git_repo, git_project_ext = os.path.splitext(git_project)

    if git_project_path == '.':
        git_project_path = git_repo
    git_project_path = os.path.expanduser(git_project_path)

    if os.path.isdir(os.path.join(git_project_path, '.git')):
        print('Pulling project', flush=True)
        subprocess.run(['git', '-C', git_project_path, 'pull', git_url, branch], check=True)
    elif os.path.isdir(git_project_path):
        git_project_path = os.path.join(git_project_path, git_repo)
        if os.path.isdir(os.path.join(git_project_path, '.git')):
            print('Pulling project', flush=True)
            subprocess.run(['git', '-C', git_project_path, 'pull', git_url, branch], check=True)
        else:
            print('Cloning project to {0}'.format(git_project_path), flush=True)
            subprocess.run(['git', 'clone', git_url, git_project_path], check=True)
    else:
        print('Cloning project to {0}'.format(git_project_path), flush=True)
        subprocess.run(['git', 'clone', git_url, git_project_path], check=True)

import ast
import os
import subprocess


def system_update(dist_upgrade=False):
    """Updates and upgrades the distro.

    Performs:
        'sudo apt-get update'
        then either
        'sudo apt-get upgrade -y'
        or
        'sudo apt-get dist-upgrade -y'

    Args:
        dist_upgrade (bool): Whether to perform 'dist-upgrade' instead of 'upgrade'.

    """
    print("\n---- System Update and Upgrade ----", flush=True)
    subprocess.run(['sudo', 'apt-get', 'update'], check=True)
    if dist_upgrade:
        subprocess.run(['sudo', 'apt-get', 'dist-upgrade', '-y'], check=True)
    else:
        subprocess.run(['sudo', 'apt-get', 'upgrade', '-y'], check=True)


def system_cleanup():
    """Clean up apt software and kernels.

    Performs:
        'sudo apt-get -y -f install'
        'sudo apt-get -y --purge autoremove'
        'sudo apt-get -y autoclean'
        'sudo apt-get -y clean'

    Raises:
        CalledProcessError: If the return code was not 0.

    """
    print("\n---- System Cleanup ----", flush=True)
    subprocess.run(['sudo', 'apt-get', '-y', '-f', 'install'], check=True)
    subprocess.run(['sudo', 'apt-get', '-y', '--purge', 'autoremove'], check=True)
    subprocess.run(['sudo', 'apt-get', '-y', 'autoclean'], check=True)
    subprocess.run(['sudo', 'apt-get', '-y', 'clean'], check=True)
    print("cleanup finished", flush=True)


def is_package_installed(package_name, show_results=False):
    """Check if a software package is installed.

    Args:
        package_name (str): The name of the package to search for.
        show_results (bool): Whether to print the results to screen.
            Defaults to False.

    Returns:
        bool: Whether or not the software package is installed.

    """
    package_name = package_name.strip().lower()
    invalid_chars = ['*', '?', '[', ']', ' ']
    if any(c in invalid_chars for c in package_name):
        raise ValueError(f"Package name cannot invalid chars: {invalid_chars}")
    result = subprocess.run(['dpkg-query', '-f', '${Package} ${Version} ${Status}\n', '-W', f'{package_name}'],
                            stdout=subprocess.PIPE)
    result_str = result.stdout.decode().strip()
    if show_results:
        print('package status:', result_str)
    install_status = result_str.split()[-1] if result_str else ''

    return True if install_status == 'installed' else False


def is_valid_package(package_name, show_results=False):
    """Check if a software package exists.

    Args:
        package_name (str): The name of the package to search for.
        show_results (bool): Whether to print the results to screen.
            Defaults to False.

    Returns:
        bool: Whether or not the software package exists.

    """
    package_name = package_name.strip().lower()
    invalid_chars = ['*', '?', '[', ']', ' ']
    if any(c in invalid_chars for c in package_name):
        raise ValueError(f"Package name cannot invalid chars: {invalid_chars}")
    result = subprocess.run(['apt-cache', 'search', f'^{package_name}$'], check=True, stdout=subprocess.PIPE)
    result_str = result.stdout.decode().strip()

    if result_str:
        if show_results:
            print('package info:', result_str)
        return True
    else:
        return False


def is_gpg_key_present(key):
    """Check to see if the GPG key exists in APT.

    Notes:
        Add a ' *' every at every 4th place in the key, turning
        1234567812345678
        into
        1234 *5678 *1234 *5678
        This way when we do 'apt-key finger', we have the regex for grep to match the full key fingerprint

    Args:
        key (str): The key to search for

    Returns:
        bool: True if the key exists, False if it does not.

    """
    # http://stackoverflow.com/questions/10055631/how-do-i-insert-spaces-into-a-string-using-the-range-function
    parsed_key = ' *'.join(key[i:i+4] for i in range(0, len(key), 4))
    process_result = subprocess.run(['apt-key finger | grep -q "{0}"'.format(parsed_key)], shell=True)

    return True if process_result.returncode == 0 else False


def get_architecture_name():
    """Get the architecture name.

    This gets the name of the architecture, such as 'amd64' or 'armhf'.

    Returns:
        str: The name of the architecture.

    """
    architecture_name = subprocess.check_output(['dpkg', '--print-architecture']).decode('utf-8').strip()
    return architecture_name


def get_distribution_name():
    """Get the distribution name.

    This gets the name of the distribution, such as 'ubuntu', 'debian', or 'raspbian'.

    Returns:
        str: The name of the distribution.

    """
    distribution_name = None
    with open('/etc/os-release', 'r') as f:
        for line in f:
            if line.startswith('ID='):
                distribution_name = line[len('ID='):]
                break
    try:
        distribution_name = distribution_name.strip().lower()
    except AttributeError:
        raise AttributeError('The distribution name cannot be None')
    return distribution_name


def get_distribution_version_name():
    """Get the distribution version.

    This gets the version name of the distribution, such as 'bionic' (Ubuntu) or 'buster' (Debian).

    Returns:
        str: The version name of the distribution.

    """
    distribution_version_name = subprocess.check_output(['lsb_release', '-cs'], encoding='utf-8')
    try:
        distribution_version_name = distribution_version_name.strip().lower()
    except AttributeError:
        raise AttributeError('The distribution version name cannot be None')

    return distribution_version_name


def enable_ipv4_preference():
    """Have OS prefer IPv4.

    Enable IPv4 preference for websites that prefer IPv4.
    Notes:
        At the time updates from us.archive.ubuntu.com were very slow if IPv6 was the preference.

    """
    print("\nPrefer IPv4 in '/etc/gai.conf'", flush=True)
    subprocess.run(['sudo', 'sed', '-i',
                    r's/^#precedence ::ffff:0:0\/96  100/precedence ::ffff:0:0\/96  100/', '/etc/gai.conf'],
                   check=True)


def enable_ipv6_preference():
    """Have OS prefer IPv6.

    This simply enables IPv6 and disables the IPv4 preference, which is the default.

    """
    print("\nPrefer IPv6 in '/etc/gai.conf'", flush=True)
    subprocess.run(['sudo', 'sed', '-i',
                    r's/^precedence ::ffff:0:0\/96  100/#precedence ::ffff:0:0\/96  100/', '/etc/gai.conf'],
                   check=True)


def set_apport(enable):
    """Enable or disable the apport service.

    This will also attempt to start or stop the service.

    Args:
        enable (bool): True to enable and False to disable.

    """
    # Check that the file exists AND that a string that starts with 'enabled' exists
    subprocess.run(['grep', '-q', '^enabled=', '/etc/default/apport'], check=True)

    if enable:
        print("Enable apport", flush=True)
        subprocess.run(['sudo', 'sed', '-i', r's/^enabled\=.*$/enabled\=1/', '/etc/default/apport'], check=True)
        # This tends to error out. Apport will be enabled on restart, so don't 'check=True' this command
        subprocess.run(['sudo', 'service', 'apport', 'start'])
    else:
        print("Disable apport", flush=True)
        subprocess.run(['sudo', 'sed', '-i', r's/^enabled\=.*$/enabled\=0/', '/etc/default/apport'], check=True)
        # This tends to error out. Apport will be disabled on restart, so don't 'check=True' this command
        subprocess.run(['sudo', 'service', 'apport', 'stop'])


class GSettingsUtils:
    """Class containing utility functions for editing Gnome settings."""

    @staticmethod
    def get_gsetting(path, key):
        """Get the value of a Gsettings key.

        Notes:
            This returns the str interpretation of the value. Converting this into
            an array or numeric value must be handled by calling function. A good
            way to do this is with ast.literal_eval(), unless the value is supposed
            to be a string, in which case a ValueError is thrown.

        Args:
            path (str): The gsettings path, ie org.gnome.shell
            key (str): The gsettings key, ie favorites

        Returns:
            str: The str interpretation of the value.

        """
        print("Getting '{0}' key '{1}'".format(path, key), flush=True)
        gsettings_value = subprocess.run(['gsettings', 'get', '{0}'.format(path), '{0}'.format(key)], check=True,
                                         stdout=subprocess.PIPE)
        gsettings_value_str = gsettings_value.stdout.decode().strip()

        return gsettings_value_str

    @staticmethod
    def set_gsetting(path, key, value):
        """Set the value of a Gsettings key.

        Args:
            path (str): The gsettings path, ie com.canonical.indicator.datetime
            key (str): The gsettings key, ie show-day
            value (str): The value to set it to, ie true

        """
        print("Setting '{0}' key '{1}' to '{2}'".format(path, key, value), flush=True)
        subprocess.run(['gsettings', 'set', '{0}'.format(path), '{0}'.format(key), '{0}'.format(value)], check=True)

    @staticmethod
    def reset_gsetting(path, key):
        """Reset the value of a Gsettings key.

        Args:
            path (str): The gsettings path, ie com.canonical.indicator.datetime
            key (str): The gsettings key, ie show-day

        """
        print("Resetting '{0}' key '{1}'".format(path, key), flush=True)
        subprocess.run(['gsettings', 'reset', '{0}'.format(path), '{0}'.format(key)], check=True)


class DconfUtils:
    """Class containing utility functions for Dconf settings."""

    @staticmethod
    def load_dconf(dconf_file):
        """Load a dconf backup file.

        Loads dconf settings via a call to dconf.
        The bash equivalent is:
            dconf load / < dconf_file

        Args:
            dconf_file (str): The path to the file to load.

        """
        subprocess.run(['dconf load / < {0}'.format(dconf_file)], shell=True, check=True)

    @staticmethod
    def backup_dconf(dconf_file, overwrite=True):
        """Backup the dconf settings to a backup file.

        Creates a backup of the dconf settings via a call to dconf.
        The bash equivalent is:
            dconf dump / > dconf_file

        Args:
            dconf_file (str): The path to the backup file to write to.
            overwrite (bool): Whether to overwrite an existing file. Defaults to True.

        """
        # Create backup directory, if necessary
        dconf_file = os.path.expanduser(dconf_file)
        dconf_dir = os.path.dirname(dconf_file)
        if not os.path.exists(dconf_dir):
            os.makedirs(dconf_dir, exist_ok=True)

        # Overwrite file if it exists
        if overwrite and os.path.exists(dconf_file):
            os.remove(dconf_file)

        subprocess.run('dconf dump / > {0}'.format(dconf_file), check=True, shell=True)
        print("dconf file backed up to {0}".format(dconf_file), flush=True)

    @staticmethod
    def reset_dconf():
        """Reset the dconf settings.

        Essentially resets the settings via a call to dconf.
        The bash equivalent is:
            dconf reset -f /

        """
        print("\n---- Reset dconf ----", flush=True)
        subprocess.run(['dconf', 'reset', '-f', '/'], check=True)
        print("dconf has been reset", flush=True)


class GnomeLauncherFavoriteUtils:
    """Class containing utility functions for editing Gnome Launcher favorites."""

    @staticmethod
    def set_launcher_favorites(favorites=None):
        """Set the Launcher favorites.

        Args:
            favorites (list): A list of launcher favorites.

        """
        if not favorites:
            favorites = list()

        print("\nSetting the Launcher favorites", flush=True)
        GSettingsUtils.set_gsetting('org.gnome.shell', 'favorite-apps', str(favorites))

    @staticmethod
    def remove_launcher_favorites(favorites):
        """Remove the selected Launcher favorites.

        Args:
            favorites (list): A list of launcher favorites to remove.

        """
        print(f"\nRemoving the selected Launcher favorites: {favorites}", flush=True)
        current_favorites_str = GSettingsUtils.get_gsetting('org.gnome.shell', 'favorite-apps')
        current_favorites = ast.literal_eval(current_favorites_str)
        updated_favorites = [fav for fav in current_favorites if fav not in favorites]
        GnomeLauncherFavoriteUtils.set_launcher_favorites(updated_favorites)

    @staticmethod
    def reset_launcher_favorites():
        """Reset the Launcher favorites."""
        print("\nResetting the Unity Launcher favorites", flush=True)
        GSettingsUtils.reset_gsetting('org.gnome.shell', 'favorite-apps')

import os
import re
import subprocess
import sys


def install_direnv(apt_update=True, auto_activate=True):
    """Install direnv.

    Args:
        apt_update (bool): Whether to run 'sudo apt-get update'. Defaults to True.
        auto_activate (bool): Whether to automatically activate direnv. Defaults to True.

    """
    print("\n---- Installing direnv ----", flush=True)
    if apt_update:
        subprocess.run(['sudo', 'apt-get', 'update'], check=True)
    # Install direnv
    subprocess.run(['sudo', 'apt-get', 'install', 'direnv', '-y'], check=True)
    # Write the autoactivate settings
    if auto_activate:
        # Now write out the settings so that direnv can be automatically activated
        write_direnv_settings()
    print("Finished installing direnv", flush=True)
    print("Now run 'exec $SHELL' or open a new terminal.", flush=True)


def uninstall_direnv():
    """Remove direnv settings from bash and delete root directory."""
    print("\n---- Uninstalling direnv ----", flush=True)
    remove_direnv_settings()
    subprocess.run(['sudo', 'apt-get', 'remove', 'direnv'], check=True)
    print("Finished uninstalling direnv", flush=True)


def write_direnv_settings():
    """Write the direnv settings.

    Writes the direnv settings to '.bash_extra', if it exists; otherwise
    it writes them to '.bashrc'.

    """
    remove_direnv_settings()
    direnv_settings = ['eval "$(direnv hook bash)"']
    # Find the settings file. Either '.bash_extra' or '.bashrc' if not
    home_path = os.path.expanduser('~')
    direnv_settings_file = os.path.join(home_path, '.bash_extra')
    if not os.path.isfile(direnv_settings_file):
        direnv_settings_file = os.path.join(home_path, '.bashrc')
    if os.path.isfile(direnv_settings_file):
        print(f"Writing direnv settings to {direnv_settings_file}")
    else:
        raise ValueError("No direnv settings file found")

    with open(direnv_settings_file, 'a') as f:
        f.writelines(direnv_settings)
        f.write('\n')


def remove_direnv_settings():
    """Remove any direnv settings from bash files."""
    # The pattern to match for
    pattern = re.compile('.*eval *" *\$\(direnv hook bash\) *" *\\n')

    # Find the settings file. Either '.bash_extra' or '.bashrc' if not
    home_path = os.path.expanduser('~')
    direnv_settings_file = os.path.join(home_path, '.bash_extra')
    if not os.path.isfile(direnv_settings_file):
        direnv_settings_file = os.path.join(home_path, '.bashrc')
    if os.path.isfile(direnv_settings_file):
        print(f"Removing direnv settings from {direnv_settings_file}")
    else:
        raise ValueError("No direnv settings file found")

    with open(direnv_settings_file, 'r+') as f:
        # Read in the direnv settings file, ie the file we write the direnv settings to
        file_lines = f.readlines()

        # Collect the lines to be written
        new_file_lines = list()
        last_written_line = None
        for file_line in file_lines:
            current_line = file_line.strip(' ')
            # This avoids rewriting multiple blank lines
            if current_line == '\n' and last_written_line == '\n':
                pass
            # This makes sure we do not write the direnv settings
            elif not pattern.search(file_line):
                new_file_lines.append(file_line)
                last_written_line = current_line

        if new_file_lines and new_file_lines[-1].strip(' ') != '\n':
            new_file_lines.append('\n')

        # Go back to the beginning of the file and overwrite it with the new settings.
        f.seek(0)
        f.writelines(new_file_lines)
        f.truncate()


if __name__ == '__main__':
    """The 'main' program.

    Allowed System Parameters:
        --no-apt-update
        --no-auto-activate

    """
    # TODO: Change this to use argparse
    if '--uninstall' in sys.argv and len(sys.argv) > 2:
        raise ValueError("The '--uninstall' param cannot be used with other params")

    if '--uninstall' in sys.argv:
        uninstall_direnv()
    else:
        set_apt_update = False if '--no-apt-update' in sys.argv else True
        default_activate = False if '--no-auto-activate' in sys.argv else True
        install_direnv(apt_update=set_apt_update, auto_activate=default_activate)

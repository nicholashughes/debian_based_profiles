import subprocess

from os_utils import is_package_installed


def install_google_chrome():
    """Install Google Chrome."""
    print("\nInstall Google Chrome", flush=True)
    if not is_package_installed('gdebi'):
        print("Installing 'gdebi' as this is required.", flush=True)
        subprocess.run(['sudo', 'apt-get', 'update'], check=True)
        subprocess.run(['sudo', 'apt-get', 'install', '-y', 'gdebi'], check=True)
    if not is_package_installed('curl'):
        print("Installing 'curl' as this is required.", flush=True)
        subprocess.run(['sudo', 'apt-get', 'update'], check=True)
        subprocess.run(['sudo', 'apt-get', 'install', '-y', 'curl'], check=True)
    # Download and install Google Chrome
    subprocess.run([f'curl https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -o ~/google-chrome-stable_current_amd64.deb -s'],
                   shell=True, check=True)
    subprocess.run(['sudo gdebi --non-interactive ~/google-chrome-stable_current_amd64.deb'], shell=True,
                   check=True)
    subprocess.run(['rm -f ~/google-chrome-stable_current_amd64.deb'], shell=True, check=True)


def uninstall_google_chrome():
    """Uninstall Google Chrome."""
    print("\nInstall Google Chrome", flush=True)
    subprocess.run(['sudo', 'apt-get', 'purge', 'google-chrome-stable'], check=True)
    subprocess.run(['rm', '~/.config/google-chrome', '-rf'], check=True)


if __name__ == '__main__':
    """The 'main' program."""
    # TODO: Change this to use argparse
    install_google_chrome()

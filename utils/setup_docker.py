import os
import sys
import subprocess

from git_utils import get_latest_github_release_version
from os_utils import is_gpg_key_present, get_distribution_name, get_distribution_version_name


def install_docker(apt_update=True,
                   create_docker_group=True,
                   enable_ufw_forwarding=True,
                   include_docker_compose=True):
    """Install Docker.

    Installs docker-ce and optionally docker-compose.

    Args:
        apt_update (bool): Whether or not to update with a 'sudo apt-get update' command.
            Defaults to True.
        create_docker_group (bool): Whether to create a 'Docker' group and add the current
            user to it. Defaults to True.
        enable_ufw_forwarding (bool): Whether to enable UFW forwarding. Defaults to True.
        include_docker_compose (bool): Whether to install docker-compose as well. Defaults to True.

    """
    print("\n---- Install Docker ----", flush=True)
    docker_check = subprocess.run(['docker --version'], shell=True)
    # If the return code > 0, then the command failed and Docker is not installed
    if docker_check.returncode:
        install_docker_engine(apt_update=apt_update,
                              create_docker_group=create_docker_group,
                              enable_ufw_forwarding=enable_ufw_forwarding)
        if include_docker_compose:
            install_docker_compose()
        print("Finished installing Docker", flush=True)
    else:
        print('Error: Docker is already installed - skipping installation', flush=True)


def uninstall_docker():
    """Uninstall Docker.

    Uninstalls docker-ce and docker-compose.

    Note: This does not delete user created configuration files; those must be deleted manually.

    """
    print("\n---- Uninstalling Docker ----", flush=True)
    uninstall_docker_engine()
    uninstall_docker_compose()
    _remove_docker_group()
    _disable_ufw_forwarding()
    print("Finished uninstalling Docker", flush=True)


def install_docker_engine(install_via_docker_script=True,
                          docker_version=None,
                          create_docker_group=True,
                          enable_ufw_forwarding=True,
                          apt_update=True):
    """Install docker-ce.

    Args:
        install_via_docker_script (bool): Whether to install via the official Docker installation
            script. Defaults to True.
        docker_version (str): The Docker version to install. If it is None, it will use the latest
            version.
        create_docker_group (bool): Whether to create a 'Docker' group and add the current
            user to it. Defaults to True.
        enable_ufw_forwarding (bool): Whether to enable UFW forwarding. Defaults to True.
        apt_update (bool): Whether or not to update with a 'sudo apt-get update' command.
            Defaults to True.

    """
    print("\n---- Install docker-ce ----", flush=True)
    if apt_update:
        subprocess.run(['sudo', 'apt-get', 'update'], check=True)

    # Install prereqs
    _get_docker_engine_prerequisites()

    # Now install docker
    if install_via_docker_script:
        print("\nInstalling Docker via official script", flush=True)
        if docker_version:
            os.environ['VERSION'] = docker_version
        subprocess.run(['curl -fsSL https://get.docker.com | sh'], shell=True, check=True)
    else:
        print("\nInstalling Docker via local script", flush=True)
        _add_docker_gpg_key()
        _add_docker_to_apt_sources()
        if docker_version:
            subprocess.run(['sudo', 'apt-get', 'install', '-y',
                            'docker-ce={0}'.format(docker_version),
                            'docker-ce-cli={0}'.format(docker_version),
                            'containerd.io'], check=True)
        else:
            subprocess.run(['sudo', 'apt-get', 'install', '-y', 'docker-ce', 'docker-ce-cli', 'containerd.io'],
                           check=True)

    # Don't check the result of start, b/c if it is already running, it will fail
    subprocess.run(['sudo', 'systemctl', 'start', 'docker.service'], check=False)
    process_result = subprocess.run(['sudo service docker status | grep "Active: active (running)"'], shell=True,
                                    check=False)
    if process_result.returncode == 0:
        subprocess.run(['sudo', 'docker', 'run', 'hello-world'], check=True)
    else:
        print("WARNING: Did not test 'sudo docker run hello-world'. The docker service is not running and should be.",
              flush=True)
    subprocess.run(['sudo', 'docker', 'info'], check=True)

    # Create a docker group and add the current user to it so that 'sudo' for Docker commands is not necessary
    if create_docker_group:
        _create_docker_group()

    if enable_ufw_forwarding:
        _enable_ufw_forwarding()

    # Enable docker to autostart
    print("\nEnable Docker autostart", flush=True)
    subprocess.run(['sudo', 'systemctl', 'enable', 'docker'], check=True)

    print("Finished installing docker-ce", flush=True)


def uninstall_docker_engine():
    """Uninstall Docker Engine.

    Note: This does not delete user created configuration files; those must be deleted manually.

    """
    print("\n---- Uninstalling docker-ce ----", flush=True)
    subprocess.run(['sudo', 'apt-get', 'purge', '-y', 'docker-ce'], check=True)
    subprocess.run(['rm', '-rf', '/var/lib/docker'], check=True)
    print("Finished uninstalling docker-ce", flush=True)


def install_docker_compose(version=None):
    """Install the latest Docker Compose.

    This will install Docker Compose. If no version is selected, it will install the lastest version.

    Args:
        version (str): The Docker compose version to install.

    """
    print("\n---- Installing docker-compose ----", flush=True)
    if not version:
        # Get the latest released version
        version = get_latest_github_release_version('https://github.com/docker/compose.git')
    # Download the latest version
    compose_url = 'https://github.com/docker/compose/releases/download/{0}/' \
                  'docker-compose-$(uname -s)-$(uname -m)'.format(version)
    command = 'curl -L {0} > /usr/local/bin/docker-compose'.format(compose_url)
    subprocess.run(['sudo', 'sh', '-c', command], check=True)
    # Make sure it is executable
    subprocess.run(['sudo', 'chmod', '+x', '/usr/local/bin/docker-compose'], check=True)
    # Add in Bash completion
    command = 'sudo curl -L ' \
              'https://raw.githubusercontent.com/docker/compose/{0}/contrib/completion/bash/docker-compose > ' \
              '/etc/bash_completion.d/docker-compose'.format(version)
    subprocess.run(['sudo', 'sh', '-c', command], check=True)
    print("Finished installing docker-compose", flush=True)


def uninstall_docker_compose():
    """Uninstall Docker Compose."""
    print("\n---- Uninstalling docker-compose ----", flush=True)
    subprocess.run(['sudo', 'rm', '/usr/local/bin/docker-compose'], check=True)
    print("Finished uninstalling docker-compose", flush=True)


def _get_docker_engine_prerequisites():
    """Get the prerequisites and dependencies for docker-ce.

    According to the installation instructions, Debian should install gnupg2 and
    Ubuntu should install gnupg-agent. The installation script only installs gnupg.

    The gnupg2 package is a more modular version of the GnuPG software. Debian, Ubuntu,
    and Raspbian are all capable of installing gnupg2 as well as gnupg-agent.


    """
    distribution_name = get_distribution_name()

    print("\nInstalling prerequisites", flush=True)
    if distribution_name not in ['debian', 'ubuntu', 'raspbian']:
        raise ValueError("Unsupported distribution for this script - try the script at get.docker.io")

    subprocess.run(['sudo', 'apt-get', 'install', '-y', 'apt-transport-https', 'ca-certificates', 'curl',
                    'gnupg2', 'gnupg-agent', 'software-properties-common'], check=True)


def _add_docker_gpg_key():
    """Add the Docker GPG key."""
    distribution_name = get_distribution_name()

    # Check to see if we already have the Docker GPG key. If we do not, then download it
    docker_gpg_key = is_gpg_key_present('9DC858229FC7DD38854AE2D88D81803C0EBFCD88')
    if docker_gpg_key:
        print("Docker GPG key already present", flush=True)
    else:
        try:
            subprocess.run(['curl -fsSL https://download.docker.com/linux/{0}/gpg | '
                            'sudo apt-key add -'.format(distribution_name)],
                           shell=True, check=True)
        except subprocess.CalledProcessError:
            print("If this is a Virtualbox Environment, the NAT settings need to be set to 'bridged'."
                  "\nThis is an issue with VirtualBox version 5."
                  "\nSee here:"
                  "\n    https://forums.docker.com/t/gpg-key-for-docker-repo-fail-to-fetch-from-key-server/24253",
                  flush=True)
            raise


def _add_docker_to_apt_sources():
    """Add Docker to the APT sources list."""
    distribution_name = get_distribution_name()
    distribution_version_name = get_distribution_version_name()

    # Add the repo to our APT sources list - add-apt-repository fails in Raspbian
    subprocess.run('sudo rm /etc/apt/sources.list.d/docker.list', shell=True)
    apt_string = 'deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/{0} {1} stable'.format(
        distribution_name, distribution_version_name)
    subprocess.run(['echo "{0}" | sudo tee -a /etc/apt/sources.list.d/docker.list'.format(apt_string)],
                   shell=True, check=True)

    print("Running 'sudo apt-get update -qq'. Please wait.", flush=True)
    subprocess.run(['sudo', 'apt-get', 'update', '-qq'], check=True)

    # Verify that APT is pulling from the right repository
    subprocess.run(['apt-cache', 'policy', 'docker-ce'], check=True)


def _create_docker_group():
    """Create a 'Docker' group and add the current user.

    Note: The benefit of this is being able to issue Docker commands
        without having to use 'sudo' everytime.

    """
    print("\nCreating a 'docker' group and adding the current user.", flush=True)
    subprocess.run(['sudo', 'groupadd', '-f', 'docker'], check=True)
    subprocess.run(['sudo usermod -aG docker ${USER}'], shell=True, check=True)
    print("NOTE: Logout and login to be sure the user has their groups updated.", flush=True)


def _remove_docker_group():
    """Remove the 'Docker' group."""
    print("\nRemoving the 'docker' group.", flush=True)

    # Remove current user from docker group
    subprocess.run(["sudo deluser ${USER} docker"], shell=True, check=False)
    # Check if other members are in that group
    members_output = subprocess.run(["getent group docker | awk -F: '{print $NF}'"], shell=True, check=True,
                                    stdout=subprocess.PIPE)
    members = members_output.stdout.decode().strip().split()
    if members:
        print(f"Group 'docker' not deleted as it contains users: {members}")
    else:
        try:
            subprocess.run(['sudo', 'groupdel', 'docker'], check=True, stderr=subprocess.PIPE)
        except subprocess.CalledProcessError as err:
            err_str = err.stderr.decode().strip()
            if not err_str.endswith("'docker' does not exist"):
                raise
        else:
            print("NOTE: Logout and login to be sure the user has their groups updated.", flush=True)


def _enable_ufw_forwarding():
    """Enable UFW forwarding for Docker.

    Note:
        https://docs.docker.com/engine/installation/linux/ubuntulinux/#/enable-ufw-forwarding

    """
    _set_ufw_forwarding(True)


def _disable_ufw_forwarding():
    """Disable UFW forwarding for Docker.

    Note:
        https://docs.docker.com/engine/installation/linux/ubuntulinux/#/enable-ufw-forwarding

    """
    _set_ufw_forwarding(False)


def _set_ufw_forwarding(enable):
    """Whether to enable or disable UFW forwarding for Docker.

    Note:
        https://docs.docker.com/engine/installation/linux/ubuntulinux/#/enable-ufw-forwarding

    Args:
        enable (bool): Whether to enable or disable UFW forwarding for Docker.

    """
    # Check if IFW is even enabled
    ufw_status = subprocess.run(['sudo', 'ufw', 'status'], stdout=subprocess.PIPE)
    if ufw_status.returncode != 0:
        print("WARNING: UFW is not installed. No changes will be made.", flush=True)
        return
    elif 'inactive' in ufw_status.stdout.decode().strip():
        print("WARNING: UFW is not active. No changes will be made.", flush=True)
        return

    # Either "ACCEPT" or "DROP" forwarding
    if enable:
        print("\nEnabling UFW forwarding", flush=True)
        subprocess.run(['sudo', 'sed', '-i', 's/^DEFAULT_FORWARD_POLICY=.*/DEFAULT_FORWARD_POLICY="ACCEPT"/',
                        '/etc/default/ufw'], check=True)
    else:
        print("\nDisabling UFW forwarding", flush=True)
        subprocess.run(['sudo', 'sed', '-i', 's/^DEFAULT_FORWARD_POLICY=.*/DEFAULT_FORWARD_POLICY="DROP"/',
                        '/etc/default/ufw'], check=True)
    # Reload UFW
    subprocess.run(['sudo', 'ufw', 'reload'], check=True)
    # Enable the Docker Port
    if enable:
        subprocess.run(['sudo', 'ufw', 'allow', '2375/tcp'], check=True)
    else:
        subprocess.run(['sudo', 'ufw', 'deny', '2375/tcp'], check=True)


if __name__ == '__main__':
    """The 'main' program.

    Allowed System Parameters:
        --no-apt-update
        --no-docker-group
        --no-ufw-forwarding
        --no-docker-compose

    """
    # TODO: Change this to use argparse
    if '--uninstall' in sys.argv and len(sys.argv) > 2:
        raise ValueError("The '--uninstall' param cannot be used with other params")

    if '--uninstall' in sys.argv:
        uninstall_docker()
    else:
        # Grab the parameters to determine the Docker install options
        opt_apt_update = False if '--no-apt-update' in sys.argv else True
        opt_create_docker_group = False if '--no-docker-group' in sys.argv else True
        opt_enable_ufw_forwarding = False if '--no-ufw-forwarding' in sys.argv else True
        opt_include_docker_compose = False if '--no-docker-compose' in sys.argv else True
        # Now install Docker
        install_docker(apt_update=opt_apt_update,
                       create_docker_group=opt_create_docker_group,
                       enable_ufw_forwarding=opt_enable_ufw_forwarding,
                       include_docker_compose=opt_include_docker_compose)

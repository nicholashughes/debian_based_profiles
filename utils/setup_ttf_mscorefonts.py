import os
import subprocess
import sys

from os_utils import is_package_installed


def install_ttf_mscorefonts(version='3.7'):
    """Install the the Microsoft Core True Type Fonts.

    Notes:
        The individual font files are in fonts. The link inside the .deb files used to be broken,
        so it was necessary to install them by hand. Now this has been resolved, but if the TTF
        fonts need to be installed manually, then 'sudo dpkg-reconfigure ttf-mscorefonts-installer'
        and selecting the font files in 'data/fonts' will work.

    Args:
        version (str): The version to install. Defaults to 3.7.

    """
    if not is_package_installed('curl'):
        print("Installing 'curl' as this is required.", flush=True)
        subprocess.run(['sudo', 'apt-get', 'update'], check=True)
        subprocess.run(['sudo', 'apt-get', 'install', '-y', 'curl'], check=True)
    if not is_package_installed('gdebi'):
        print("Installing 'gdebi' as this is required.", flush=True)
        subprocess.run(['sudo', 'apt-get', 'update'], check=True)
        subprocess.run(['sudo', 'apt-get', 'install', '-y', 'gdebi'], check=True)

    print(f"Installing ttf-mscorefonts-installer version '{version}'")
    # Get the path to the local file
    ttf_file_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
                                 f'data/ttf-mscorefonts-installer_{version}_all.deb')
    # If we don't have it, download it
    if not os.path.isfile(ttf_file_path):
        ttf_file_path = f'ttf-mscorefonts-installer_{version}_all.deb'
        subprocess.run([f'curl http://ftp.de.debian.org/debian/pool/contrib/m/msttcorefonts/ttf-mscorefonts-installer_{version}_all.deb -o ~/{ttf_file_path}'],
                       shell=True,
                       check=True)
        ttf_file_path = f'~/ttf-mscorefonts-installer_{version}_all.deb'

    # Remove the previous installation and any failed installations
    subprocess.run(['sudo rm -rf /var/lib/update-notifier/package-data-downloads/partial/*'], shell=True, check=True)
    subprocess.run(['sudo apt-get -y remove --purge ttf-mscorefonts-installer'], shell=True, check=True)

    # Install the new version
    subprocess.run([f'sudo gdebi --non-interactive {ttf_file_path}'], shell=True, check=True)

    # This will bring up the GUI to install the TTF fonts if the above failed.
    # On the first screen, type in the full path to the fonts and then everything will work.
    # The fonts are in the 'data/fonts' directory.
    subprocess.run(['sudo dpkg-reconfigure ttf-mscorefonts-installer'], shell=True, check=True)


# TODO: Add an uninstaller


if __name__ == '__main__':
    """The 'main' program.

    Allowed System Parameters:
        --version

    """
    # TODO: Switch this to argparse
    ttf_version = None
    if '--version' in sys.argv:
        ttf_version = sys.argv[2]

    if ttf_version:
        install_ttf_mscorefonts(ttf_version)
    else:
        install_ttf_mscorefonts()

import os
import re


def enable_numlock_on_login():
    """Enable numlock automatically when logging in.

    This command will enable the numlock:
        /usr/bin/setleds -D +num

    Issuing this command in '.bash_extra' or '.bashrc' will then
    automatically enable numlock once the user logs in.

    """
    remove_numlock_on_login()

    # Write the settings to either '.bash_extra' or '.bashrc'
    home_path = os.path.expanduser('~')
    numlock_settings_file = os.path.join(home_path, '.bash_extra')
    if not os.path.isfile(numlock_settings_file):
        numlock_settings_file = os.path.join(home_path, '.bashrc')
    if os.path.isfile(numlock_settings_file):
        print(f"Writing to {numlock_settings_file}")
    else:
        raise ValueError("No .bash_extra or .bashrc file found")

    with open(numlock_settings_file, 'a') as f:
        f.writelines('/usr/bin/setleds -D +num')
        f.write('\n')


def remove_numlock_on_login():
    """Remove the numlock enable settings from bash files."""
    # The pattern to match for
    pattern = re.compile(r""".*/usr/bin/setleds *-D  *\+num *\n""")

    # Remove the settings from either '.bash_extra' or '.bashrc'
    home_path = os.path.expanduser('~')
    numlock_settings_file = os.path.join(home_path, '.bash_extra')
    if not os.path.isfile(numlock_settings_file):
        numlock_settings_file = os.path.join(home_path, '.bashrc')
    if os.path.isfile(numlock_settings_file):
        print(f"Removing numlock settings from {numlock_settings_file}")
    else:
        raise ValueError("No .bash_extra or .bashrc file found")

    with open(numlock_settings_file, 'r+') as f:
        # Read in the settings file, ie the file we write the settings to
        file_lines = f.readlines()

        # Collect the lines to be written
        new_file_lines = list()
        last_written_line = None
        for file_line in file_lines:
            current_line = file_line.strip(' ')
            # This avoids rewriting multiple blank lines
            if current_line == '\n' and last_written_line == '\n':
                pass
            # Now we overwrite the file with the current line if it is not a pattern match
            elif not pattern.search(file_line):
                new_file_lines.append(file_line)
                last_written_line = current_line

        if new_file_lines and new_file_lines[-1].strip(' ') != '\n':
            new_file_lines.append('\n')

        # Go back to the beginning of the file and overwrite it with the new settings.
        f.seek(0)
        f.writelines(new_file_lines)
        f.truncate()

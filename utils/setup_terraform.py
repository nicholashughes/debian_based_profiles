#!/usr/bin/env python3
import argparse
import subprocess

from os_utils import get_architecture_name


def parse_arguments():
    """Parse any arguments given for the script."""

    description = "Install or uninstall Terraform."
    parser = argparse.ArgumentParser(description=description, add_help=False)
    parser.add_argument('-h', '--help', action='help', help="Show this help message and exit.")
    subparsers = parser.add_subparsers(help='Help with commands.', dest='subparser_name')

    # Install parser
    install_parser = subparsers.add_parser('install', help="Install Terraform.", add_help=False)
    install_parser.add_argument('-h', '--help', action='help', help="Show this help message and exit.")
    install_parser.add_argument('-v', '--version', default='latest', help="The version to install.")
    install_parser.add_argument('-l', '--location', default='/usr/local/bin', help="The location for where to install.")

    # Remove parser
    remove_parser = subparsers.add_parser('remove', help="Uninstall Terraform.", add_help=False)
    remove_parser.add_argument('-h', '--help', action='help', help="Show this help message and exit.")

    param_args = parser.parse_args()

    return param_args


def install_terraform(terraform_version='latest', install_location='/usr/local/bin', apt_update=False):
    """Install Terraform.

    Installs terraform to the INSTALL_LOCATION. By default it installs the latest version

    Args:
        terraform_version (str): The version of Terraform to install. Defaults to 'latest',
            which installs the latest version.
        install_location (str): Where to install Terraform. Defaults to '/usr/local/bin'.
        apt_update (bool): Whether or not to update with a 'sudo apt-get update' command.
            Defaults to False.

    """
    # Install script requirements
    if apt_update:
        subprocess.run(['sudo', 'apt-get', 'update'], check=True)
    subprocess.run(['sudo', 'apt-get', 'install', '-y', 'curl', 'unzip'], check=True)

    # Get latest version number
    if terraform_version == 'latest':
        terraform_version = get_latest_version_of_terraform()

    # Build out the download URL
    architecture_name = get_architecture_name()
    if architecture_name == 'i386':
        terraform_url = f'https://releases.hashicorp.com/terraform/{terraform_version}/terraform_{terraform_version}_linux_i386.zip'
    elif architecture_name == 'amd64':
        terraform_url = f'https://releases.hashicorp.com/terraform/{terraform_version}/terraform_{terraform_version}_linux_amd64.zip'
    elif architecture_name == 'armhf':
        terraform_url = f'https://releases.hashicorp.com/terraform/{terraform_version}/terraform_{terraform_version}_linux_arm.zip'
    else:
        raise ValueError(f"Unsure which version of Terraform to install with architecture {architecture_name}")

    # Download and install Terraform
    print("\n---- Install Terraform ----", flush=True)
    subprocess.run([f'curl {terraform_url} -o ~/terraform.zip -s'], shell=True, check=True)
    subprocess.run(['unzip -o ~/terraform.zip -d ~/'], shell=True, check=True)
    subprocess.run([f'sudo mv ~/terraform {install_location}'], shell=True, check=True)
    subprocess.run(['rm -f ~/terraform.zip'], shell=True, check=True)
    print("Finished installing Terraform", flush=True)


def uninstall_terraform():
    """Uninstall Terraform.

    Uninstalls terraform by deleting the executable. The executable
    is located by running 'which terraform'.

    """
    print("\n---- Uninstalling Terraform ----", flush=True)

    terraform_location = subprocess.run(['which', 'terraform'], stdout=subprocess.PIPE)
    terraform_location = terraform_location.stdout.decode('utf-8').strip()
    print(f'Terraform location: {terraform_location}')
    if terraform_location:
        subprocess.run(['sudo', 'rm', terraform_location], check=True)
        print("Finished uninstalling Terraform", flush=True)
    else:
        print('Terraform could not be found')


def get_latest_version_of_terraform():
    """Gets the latest version of Terraform.

    This makes a curl command to the Terraform download site:
    https://www.terraform.io/downloads.html
    In that text is the sentence:
    "Below are the available downloads for the latest version of Terraform (0.X.X)."
    The version number is then pulled out from there.

    Returns:
        str: The latest version of Terraform

    """
    terraform_download_url = 'https://www.terraform.io/downloads.html'
    terraform_grep_string = "Below are the available downloads for the latest version of Terraform"
    terraform_url_string = subprocess.check_output(
        [f'curl -s {terraform_download_url} | grep -A 1 "{terraform_grep_string}"'], shell=True).decode('utf-8').strip()
    # Get the index of the open/close parentheses, and start after the 'terraform_grep_string'
    index_open_parentheses = terraform_url_string.index('(', len(terraform_grep_string))
    index_close_parentheses = terraform_url_string.index(')', len(terraform_grep_string))
    # Cut out the version from the curl output
    terraform_version = terraform_url_string[index_open_parentheses+1:index_close_parentheses]

    return terraform_version


if __name__ == '__main__':
    """The 'main' program."""
    args = parse_arguments()
    if args.subparser_name == 'remove':
        uninstall_terraform()
    else:
        install_terraform()

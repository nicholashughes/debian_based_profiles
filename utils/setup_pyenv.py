import os
import re
import subprocess
import sys


def install_pyenv(auto_activate=True, apt_update=True):
    """Install pyenv.

    Args:
        auto_activate (bool): Whether to auto activate in bash. Defaults to True.
        apt_update (bool): Whether to run 'sudo apt-get update'. Defaults to True.

    """
    uninstall_pyenv()
    print("\n---- Installing Pyenv ----", flush=True)
    if apt_update:
        subprocess.run(['sudo', 'apt-get', 'update'], check=True)
    subprocess.run(['sudo', 'apt-get', 'install', 'curl'], check=True)
    install_pyenv_dependencies()
    subprocess.run(['curl https://pyenv.run | bash'], shell=True, check=True)
    # Install pyenv-virtualenv or remove the auto-activate part from the settings
    pyenv_settings = ['export PYENV_ROOT="$HOME/.pyenv"\n',
                      'export PATH="$PYENV_ROOT/bin:$PATH"\n',
                      'eval "$(pyenv init -)"\n',
                      'eval "$(pyenv virtualenv-init -)"\n']
    if auto_activate:
        # Now write out the settings so that pyenv can be automatically activated
        write_pyenv_settings(pyenv_settings)
    print("Finished updating Pyenv", flush=True)
    print("Now run 'exec $SHELL' or open a new terminal.", flush=True)


def uninstall_pyenv():
    """Remove pyenv settings from bash and delete root directory."""
    print("\n---- Uninstalling Pyenv ----", flush=True)
    remove_pyenv_settings()
    subprocess.run(['rm -rf `pyenv root`'], shell=True, check=True)
    print("Finished uninstalling Pyenv", flush=True)


def install_pyenv_dependencies():
    """Install pyenv dependencies.

    These just ensure that the Python environment will be robust,
    allowing packages like cryptography to be installed.

    See here:
    https://github.com/pyenv/pyenv/wiki#suggested-build-environment

    """
    print("\nInstalling python dependencies", flush=True)
    subprocess.run(['sudo', 'apt-get', 'install', '-y',
                    'make',
                    'build-essential',
                    'libssl-dev',
                    'zlib1g-dev',
                    'libbz2-dev',
                    'libreadline-dev',
                    'libsqlite3-dev',
                    'curl',
                    'wget',
                    'llvm',
                    'libncurses5-dev',
                    'xz-utils',
                    'tk-dev',
                    'libxml2-dev',
                    'libxmlsec1-dev',
                    'libffi-dev',
                    'liblzma-dev'
                    ], check=True)


def remove_pyenv_settings():
    """Remove any pyenv settings from bash files."""
    # The patterns to match for
    # TODO: escape \/.pyenv
    pattern1 = re.compile(r""".*export *PYENV_ROOT=['"]\$HOME/.pyenv[ '"]*\n""")
    pattern2 = re.compile(r""".*export *PATH=['"]\$PYENV_ROOT/bin:\$PATH[ '"]*\n""")
    pattern3 = re.compile(r""".*eval *" *\$\(pyenv init -\) *" *\n""")
    pattern4 = re.compile(r""".*eval *" *\$\(pyenv virtualenv-init -\) *" *\n""")

    # Remove the settings from either '.bash_extra' or '.bashrc'
    home_path = os.path.expanduser('~')
    pyenv_settings_file = os.path.join(home_path, '.bash_extra')
    if not os.path.isfile(pyenv_settings_file):
        pyenv_settings_file = os.path.join(home_path, '.bashrc')
    if os.path.isfile(pyenv_settings_file):
        print(f"Removing pyenv settings from {pyenv_settings_file}")
    else:
        raise ValueError("No pyenv settings file found")

    with open(pyenv_settings_file, 'r+') as f:
        # Read in the pyenv settings file, ie the file we write the pyenv settings to
        file_lines = f.readlines()

        # Collect the lines to be written
        new_file_lines = list()
        last_written_line = None
        for file_line in file_lines:
            current_line = file_line.strip(' ')
            # This avoids rewriting multiple blank lines
            if current_line == '\n' and last_written_line == '\n':
                pass
            # Now we overwrite the file with the current line if it is not a pattern match
            elif not (pattern1.search(file_line) or
                      pattern2.search(file_line) or
                      pattern3.search(file_line) or
                      pattern4.search(file_line)):
                new_file_lines.append(file_line)
                last_written_line = current_line

        if new_file_lines and new_file_lines[-1].strip(' ') != '\n':
            new_file_lines.append('\n')

        # Go back to the beginning of the file and overwrite it with the new settings.
        f.seek(0)
        f.writelines(new_file_lines)
        f.truncate()


def write_pyenv_settings(pyenv_settings):
    """Write the pyenv settings.

    Writes the pyenv settings to '.bash_extra', if it exists; otherwise
    it writes them to '.bashrc'.

    Args:
        pyenv_settings (list): A list of the strings to write to the settings file.

    """
    remove_pyenv_settings()
    # Write the settings to either '.bash_extra' or '.bashrc' if not
    home_path = os.path.expanduser('~')
    pyenv_settings_file = os.path.join(home_path, '.bash_extra')
    if not os.path.isfile(pyenv_settings_file):
        pyenv_settings_file = os.path.join(home_path, '.bashrc')
    if os.path.isfile(pyenv_settings_file):
        print(f"Writing pyenv settings to {pyenv_settings_file}")
    else:
        raise ValueError("No pyenv settings file found")

    with open(pyenv_settings_file, 'a') as f:
        f.writelines(pyenv_settings)
        f.write('\n')


if __name__ == '__main__':
    """The 'main' program.

    Allowed System Parameters:
        --no-apt-update
        --no-auto-activate

    """
    # TODO: Change this to use argparse
    if '--uninstall' in sys.argv and len(sys.argv) > 2:
        raise ValueError("The '--uninstall' param cannot be used with other params")

    if '--uninstall' in sys.argv:
        uninstall_pyenv()
    else:
        apt_update_val = False if '--no-apt-update' in sys.argv else True
        default_activate = False if '--no-auto-activate' in sys.argv else True
        install_pyenv(auto_activate=default_activate,
                      apt_update=apt_update_val)
